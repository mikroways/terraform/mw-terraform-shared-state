resource "aws_iam_policy" "this" {
  for_each = local.environment_projects
  name     = "${var.role_prefix}${title(each.value.name)}${var.role_manager_tag}${title(each.value.project.name)}"
  path     = "/"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : "s3:ListBucket",
        "Resource" : "${aws_s3_bucket.this.arn}"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        "Resource" : [
          "${aws_s3_bucket.this.arn}/${each.value.name}/${each.value.project.name}/tfstate",
          "${aws_s3_bucket.this.arn}/${each.value.name}/*/${each.value.project.name}/tfstate"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "dynamodb:DescribeTable",
          "dynamodb:DeleteItem",
          "dynamodb:GetItem",
          "dynamodb:PutItem"
        ],
        "Resource" : "${aws_dynamodb_table.this.arn}",
        "Condition" : {
          "ForAllValues:StringLike" : {
            "dynamodb:LeadingKeys" : [
              "${aws_s3_bucket.this.id}/${each.value.name}/${each.value.project.name}/tfstate",
              "${aws_s3_bucket.this.id}/${each.value.name}/${each.value.project.name}/tfstate-md5",
              "${aws_s3_bucket.this.id}/${each.value.name}/*/${each.value.project.name}/tfstate",
              "${aws_s3_bucket.this.id}/${each.value.name}/*/${each.value.project.name}/tfstate-md5"
            ]
          }
        }
      }
    ]
  })

  tags = {
    Name = "${var.role_prefix}${title(each.value.name)}${var.role_manager_tag}${title(each.value.project.name)}"
  }
}

resource "aws_iam_policy" "wildcard" {
  for_each = var.create_policy_wildcard_per_environment ? toset([for environment in var.environments : environment.name]) : []
  name     = "${var.role_prefix}${title(each.key)}${trimsuffix(var.role_manager_tag, "-")}"
  path     = "/"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : "s3:ListBucket",
        "Resource" : "${aws_s3_bucket.this.arn}"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        "Resource" : [
          "${aws_s3_bucket.this.arn}/${each.key}/*/tfstate",
          "${aws_s3_bucket.this.arn}/${each.key}/*/*/tfstate"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "dynamodb:DescribeTable",
          "dynamodb:DeleteItem",
          "dynamodb:GetItem",
          "dynamodb:PutItem"
        ],
        "Resource" : "${aws_dynamodb_table.this.arn}",
        "Condition" : {
          "ForAllValues:StringLike" : {
            "dynamodb:LeadingKeys" : [
              "${aws_s3_bucket.this.id}/${each.key}/*/tfstate",
              "${aws_s3_bucket.this.id}/${each.key}/*/tfstate-md5",
              "${aws_s3_bucket.this.id}/${each.key}/*/*/tfstate",
              "${aws_s3_bucket.this.id}/${each.key}/*/*/tfstate-md5"
            ]
          }
        }
      }
    ]
  })

  tags = {
    Name = "${var.role_prefix}${title(each.key)}${trimsuffix(var.role_manager_tag, "-")}"
  }
}
