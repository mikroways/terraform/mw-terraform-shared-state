data "aws_iam_openid_connect_provider" "this" {
  for_each = {
    for name, identity_provider in var.identity_providers :
    name => coalesce(identity_provider.host, local.identity_provider_default[identity_provider.type].host)
    if identity_provider.arn == null
  }
  url = "https://${each.value}"
}
