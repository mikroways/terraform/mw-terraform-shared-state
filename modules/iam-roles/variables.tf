variable "role_prefix" {
  type        = string
  description = "Prefix that will be added to all resources"
  default     = "MW-"
}

variable "git_repository" {
  type        = string
  default     = ""
  description = "Terraform git repository"
}

variable "project_owner" {
  type        = string
  default     = ""
  description = "Project owner name"
}

###################
# OIDC Provider
###################

variable "identity_providers" {
  type = map(object({
    type     = string
    arn      = optional(string)
    host     = optional(string)
    audience = optional(string)
  }))

  validation {
    condition     = alltrue([for id in var.identity_providers : contains(["github", "gitlab"], id.type)])
    error_message = "Valid values for identity_provider_type: 'github' or 'gitlab'"
  }
  validation {
    condition     = alltrue([for idp in var.identity_providers : length(idp.arn) > 0 if idp.arn != null])
    error_message = "The identity provider ARN couldn't be an empty string"
  }

  description = "Identity providers used"

  default = {}
}

variable "audience" {
  type        = string
  description = "Terraform role trust relationship audience"
  default     = "sts.amazonaws.com"
}

############################
# Assume role from accounts
############################
variable "assume_role_from_accounts" {
  type        = list(string)
  default     = []
  description = "List of account to allow to assume roles"
}

###################
# EKS Roles
###################

variable "eks_access_policy" {
  description = "Whether to create the EKSAccess policy"
  type        = bool
  default     = false
}

variable "eks_access_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the EKSAccess sub"
}

variable "eks_management_policy" {
  description = "Whether to create the EKSManagement policy"
  type        = bool
  default     = false
}

variable "eks_management_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the EKSManagement sub"
}

variable "eks_management_extra_actions" {
  type = map(object({
    actions   = optional(list(string))
    resources = optional(list(string))
  }))
  default     = {}
  description = "Extra statements for the EKS Management role policy"
}

############################
# Billing and Cost Explorer
###########################

variable "billing_ce_policy" {
  type        = bool
  default     = false
  description = "Whether to create the Billing and Cost Explorer policy"
}

###############
# KMS roles
###############

variable "kms_policy" {
  type        = bool
  description = "Wheter to create the KMS policy"
  default     = false
}

variable "kms_access_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the KMSAccess sub"
}

variable "kms_management_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the KMSManagement sub"
}

##################
# Networking role
##################

variable "network_management_policy" {
  type        = bool
  description = "Wheter to create the networking policy"
  default     = false
}

variable "network_management_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the NetworkManagement sub"
}

variable "network_management_extra_actions" {
  type = map(object({
    actions   = optional(list(string))
    resources = optional(list(string))
  }))
  default     = {}
  description = "Extra statements for the Network Management role policy"
}

###############
# Route53 role
###############

variable "route53_management_policy" {
  type        = bool
  description = "Wheter to create the Route53 policy"
  default     = false
}

variable "route53_management_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the Route53Management sub"
}

variable "route53_management_extra_actions" {
  type = map(object({
    actions   = optional(list(string))
    resources = optional(list(string))
  }))
  default     = {}
  description = "Extra statements for the Route53 Management role policy"
}

####################
# Roles Management #
####################

variable "roles_management_policy" {
  description = "Whether to create the RolesManagement policy"
  type        = bool
  default     = false
}

variable "roles_management_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the RolesManagement sub"
}


variable "roles_management_extra_actions" {
  type = map(object({
    actions   = optional(list(string))
    resources = optional(list(string))
  }))
  default     = {}
  description = "Extra statements for the Roles Management role policy"
}

##################
# Extra Services #
##################

variable "extra_services_policy" {
  description = "Whether to create the ExtraServices policy"
  type        = bool
  default     = false
}


variable "extra_services_policy_actions" {
  description = "Actions to add to the ExtraServices Policy (if set to [] the policy won't be created)"
  type        = list(string)
  default     = []
}

variable "extra_services_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  })
  default     = null
  description = "Repositories and branches to create the ExtraServices sub"
}

################
# Shared state #
################

variable "shared_state_policy" {
  description = "Whether to create the SharedState policy"
  type        = bool
  default     = false
}

variable "shared_state_role" {
  type = object({
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
    aws_region           = optional(string)
    aws_account          = optional(string)
    dynamodb_table_name  = optional(string)
    s3_bucket_name       = optional(string)
    oidc_provider_name   = optional(string)
  })
  default     = null
  description = "Repositories and branches to create the SharedState sub"
}

###############
# Extra roles #
###############

variable "extra_roles" {
  type = map(object({
    actions              = list(string)
    repo_paths           = optional(list(string))
    repo_branches        = optional(list(string))
    include_pull_request = bool
  }))
  default     = {}
  description = "Named extra roles with their respective actions"
}
