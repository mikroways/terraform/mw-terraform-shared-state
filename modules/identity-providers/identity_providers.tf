resource "aws_iam_openid_connect_provider" "default" {
  for_each = local.identity_providers
  url      = "https://${each.value.host}"

  client_id_list  = each.value.audiences
  thumbprint_list = each.value.thumbprints
  tags = {
    Name = each.key
  }
}
