# identity-providers

For more information about the published version, you can check the [registry](https://gitlab.com/mikroways/terraform/mw-terraform-modules/-/terraform_module_registry)


<!-- BEGIN_TF_DOCS -->
## Usage

```hcl
module "identity-providers" {
  source = "gitlab.com/mikroways/identity-providers/aws"

  identity_providers = {
    github = {
      type = "github"
      thumbprints = ["b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a"]
    }
    gitlab = {
      type = "gitlab"
      audiences = ["custom.audience"]
    }
    gitlab-example = {
      type = "gitlab"
      host = "gitlab.example"
    }
  }
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.50.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.50.0 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_audience"></a> [audience](#input\_audience) | n/a | `string` | `"sts.amazonaws.com"` | no |
| <a name="input_git_repository"></a> [git\_repository](#input\_git\_repository) | Terraform git repository | `string` | `""` | no |
| <a name="input_identity_providers"></a> [identity\_providers](#input\_identity\_providers) | Map of identity providers to set in the trust relationship | <pre>map(object({<br>    type        = string<br>    host        = optional(string)<br>    thumbprints = optional(list(string))<br>    audiences   = optional(list(string))<br>  }))</pre> | n/a | yes |
| <a name="input_project_owner"></a> [project\_owner](#input\_project\_owner) | Project owner name | `string` | `""` | no |

## Permissions

The minimum permissions to fully manage this module are the following. It is
recommended to specify the account ID and the resource names of the identity
providers.

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "IAMOpenIDConnectPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:CreateOpenIDConnectProvider",
        "iam:DeleteOpenIDConnectProvider",
        "iam:GetOpenIDConnectProvider",
        "iam:TagOpenIDConnectProvider",
        "iam:UntagOpenIDConnectProvider"
      ],
      "Resource": "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/<PROVIDER_NAME>"
    }
  ]
}
```

### Example

To create all the resources shown in the usage section you can use the following
permissions

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "IAMOpenIDConnectPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:CreateOpenIDConnectProvider",
        "iam:DeleteOpenIDConnectProvider",
        "iam:GetOpenIDConnectProvider",
        "iam:TagOpenIDConnectProvider",
        "iam:UntagOpenIDConnectProvider"
      ],
      "Resource": "arn:aws:iam::*:oidc-provider/*"
    }
  ]
}
```
<!-- END_TF_DOCS -->