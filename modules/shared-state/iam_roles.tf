resource "aws_iam_role" "this" {
  for_each = local.create_roles ? local.environment_projects : {}
  name     = "${var.role_prefix}${title(each.value.name)}${var.role_manager_tag}${title(each.value.project.name)}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringEquals = {
              "${identity_provider.host}:aud" = coalesce(each.value.audience, identity_provider.audience),
              "${identity_provider.host}:sub" = concat(
                [
                  for branch in each.value.project.git_project_branches :
                  "${identity_provider.subject_prefix}:${each.value.project.git_project_path}:${identity_provider.subject_ref}${branch}"
                ],
                identity_provider.type == "github" && each.value.project.include_pull_request ? [
                  "${identity_provider.subject_prefix}:${each.value.project.git_project_path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}${title(each.value.name)}${var.role_manager_tag}${title(each.value.project.name)}"
  }
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each   = local.create_roles ? local.environment_projects : {}
  role       = aws_iam_role.this[each.key].name
  policy_arn = aws_iam_policy.this[each.key].arn
}
