# Grafana stacks module

<!-- BEGIN_TF_DOCS -->
## Usage

```hcl
module "example" {
  source = "gitlab.com/mikroways/stack/grafana"

  client_name = "example"

  create_stack_service_account = true
  create_read_access_policies  = true
  create_write_access_policies = true
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.8.2 |
| <a name="requirement_grafana"></a> [grafana](#requirement\_grafana) | 2.18.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_grafana"></a> [grafana](#provider\_grafana) | 2.18.0 |

## Resources

| Name | Type |
|------|------|
| [grafana_cloud_access_policy.logs_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy.logs_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy.metrics_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy.metrics_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy.traces_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy.traces_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy) | resource |
| [grafana_cloud_access_policy_token.logs_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_access_policy_token.logs_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_access_policy_token.metrics_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_access_policy_token.metrics_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_access_policy_token.traces_read](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_access_policy_token.traces_write](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_access_policy_token) | resource |
| [grafana_cloud_stack.this](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_stack) | resource |
| [grafana_cloud_stack_service_account.terraform](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_stack_service_account) | resource |
| [grafana_cloud_stack_service_account_token.terraform](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/cloud_stack_service_account_token) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_access_policy_logs_label_policy"></a> [access\_policy\_logs\_label\_policy](#input\_access\_policy\_logs\_label\_policy) | Define extra label policies for the logs access policies | `list(string)` | `[]` | no |
| <a name="input_access_policy_metrics_label_policy"></a> [access\_policy\_metrics\_label\_policy](#input\_access\_policy\_metrics\_label\_policy) | Define extra label policies for the metrics access policies | `list(string)` | `[]` | no |
| <a name="input_access_policy_realm"></a> [access\_policy\_realm](#input\_access\_policy\_realm) | Define the access policy realm type | `string` | `"stack"` | no |
| <a name="input_access_policy_region"></a> [access\_policy\_region](#input\_access\_policy\_region) | Override the Access Policies region slug | `string` | `""` | no |
| <a name="input_access_policy_traces_label_policy"></a> [access\_policy\_traces\_label\_policy](#input\_access\_policy\_traces\_label\_policy) | Define extra label policies for the traces access policies | `list(string)` | `[]` | no |
| <a name="input_client_name"></a> [client\_name](#input\_client\_name) | Client name used as stack name and access policies prefix | `string` | n/a | yes |
| <a name="input_client_name_slug"></a> [client\_name\_slug](#input\_client\_name\_slug) | Client name used as stack slug | `string` | `""` | no |
| <a name="input_create_read_access_policies"></a> [create\_read\_access\_policies](#input\_create\_read\_access\_policies) | Whether to create each access policy with scope metrics:read, logs:read and traces:read | `bool` | `false` | no |
| <a name="input_create_read_logs_access_policy"></a> [create\_read\_logs\_access\_policy](#input\_create\_read\_logs\_access\_policy) | Whether to create the access policy with scope logs:read | `bool` | `false` | no |
| <a name="input_create_read_metrics_access_policy"></a> [create\_read\_metrics\_access\_policy](#input\_create\_read\_metrics\_access\_policy) | Whether to create the access policy with scope metrics:read | `bool` | `false` | no |
| <a name="input_create_read_traces_access_policy"></a> [create\_read\_traces\_access\_policy](#input\_create\_read\_traces\_access\_policy) | Whether to create the access policy with scope traces:read | `bool` | `false` | no |
| <a name="input_create_stack_service_account"></a> [create\_stack\_service\_account](#input\_create\_stack\_service\_account) | Whether to create a stack service account for terraform | `bool` | `false` | no |
| <a name="input_create_write_access_policies"></a> [create\_write\_access\_policies](#input\_create\_write\_access\_policies) | Whether to create each access policy with scope metrics:write, logs:write and traces:write | `bool` | `false` | no |
| <a name="input_create_write_logs_access_policy"></a> [create\_write\_logs\_access\_policy](#input\_create\_write\_logs\_access\_policy) | Whether to create the access policy with scope logs:write | `bool` | `false` | no |
| <a name="input_create_write_metrics_access_policy"></a> [create\_write\_metrics\_access\_policy](#input\_create\_write\_metrics\_access\_policy) | Whether to create the access policy with scope metrics:write | `bool` | `false` | no |
| <a name="input_create_write_traces_access_policy"></a> [create\_write\_traces\_access\_policy](#input\_create\_write\_traces\_access\_policy) | Whether to create the access policy with scope traces:write | `bool` | `false` | no |
| <a name="input_read_logs_access_policy_display_name"></a> [read\_logs\_access\_policy\_display\_name](#input\_read\_logs\_access\_policy\_display\_name) | Read logs access policy display name | `string` | `""` | no |
| <a name="input_read_logs_access_policy_extra_scopes"></a> [read\_logs\_access\_policy\_extra\_scopes](#input\_read\_logs\_access\_policy\_extra\_scopes) | Extra scopes to add to the read logs access policy | `list(string)` | `[]` | no |
| <a name="input_read_logs_access_policy_name"></a> [read\_logs\_access\_policy\_name](#input\_read\_logs\_access\_policy\_name) | Read logs access policy name | `string` | `"logs-read"` | no |
| <a name="input_read_logs_access_policy_token_name"></a> [read\_logs\_access\_policy\_token\_name](#input\_read\_logs\_access\_policy\_token\_name) | Read logs access policy token name | `string` | `""` | no |
| <a name="input_read_metrics_access_policy_display_name"></a> [read\_metrics\_access\_policy\_display\_name](#input\_read\_metrics\_access\_policy\_display\_name) | Read metrics access policy display name | `string` | `""` | no |
| <a name="input_read_metrics_access_policy_extra_scopes"></a> [read\_metrics\_access\_policy\_extra\_scopes](#input\_read\_metrics\_access\_policy\_extra\_scopes) | Extra scopes to add to the read metrics access policy | `list(string)` | `[]` | no |
| <a name="input_read_metrics_access_policy_name"></a> [read\_metrics\_access\_policy\_name](#input\_read\_metrics\_access\_policy\_name) | Read metrics access policy name | `string` | `"metrics-read"` | no |
| <a name="input_read_metrics_access_policy_token_name"></a> [read\_metrics\_access\_policy\_token\_name](#input\_read\_metrics\_access\_policy\_token\_name) | Read metrics access policy token name | `string` | `""` | no |
| <a name="input_read_traces_access_policy_display_name"></a> [read\_traces\_access\_policy\_display\_name](#input\_read\_traces\_access\_policy\_display\_name) | Read traces access policy display name | `string` | `""` | no |
| <a name="input_read_traces_access_policy_extra_scopes"></a> [read\_traces\_access\_policy\_extra\_scopes](#input\_read\_traces\_access\_policy\_extra\_scopes) | Extra scopes to add to the read traces access policy | `list(string)` | `[]` | no |
| <a name="input_read_traces_access_policy_name"></a> [read\_traces\_access\_policy\_name](#input\_read\_traces\_access\_policy\_name) | Read traces access policy name | `string` | `"traces-read"` | no |
| <a name="input_read_traces_access_policy_token_name"></a> [read\_traces\_access\_policy\_token\_name](#input\_read\_traces\_access\_policy\_token\_name) | Read logs access policy token name | `string` | `""` | no |
| <a name="input_region"></a> [region](#input\_region) | Stack region | `string` | `"prod-us-east-0"` | no |
| <a name="input_write_logs_access_policy_display_name"></a> [write\_logs\_access\_policy\_display\_name](#input\_write\_logs\_access\_policy\_display\_name) | Write logs access policy display name | `string` | `""` | no |
| <a name="input_write_logs_access_policy_extra_scopes"></a> [write\_logs\_access\_policy\_extra\_scopes](#input\_write\_logs\_access\_policy\_extra\_scopes) | Extra scopes to add to the write logs access policy | `list(string)` | `[]` | no |
| <a name="input_write_logs_access_policy_name"></a> [write\_logs\_access\_policy\_name](#input\_write\_logs\_access\_policy\_name) | Write logs access policy name | `string` | `"logs-write"` | no |
| <a name="input_write_logs_access_policy_token_name"></a> [write\_logs\_access\_policy\_token\_name](#input\_write\_logs\_access\_policy\_token\_name) | Write logs access policy token name | `string` | `""` | no |
| <a name="input_write_metrics_access_policy_display_name"></a> [write\_metrics\_access\_policy\_display\_name](#input\_write\_metrics\_access\_policy\_display\_name) | Write metrics access policy display name | `string` | `""` | no |
| <a name="input_write_metrics_access_policy_extra_scopes"></a> [write\_metrics\_access\_policy\_extra\_scopes](#input\_write\_metrics\_access\_policy\_extra\_scopes) | Extra scopes to add to the write metrics access policy | `list(string)` | `[]` | no |
| <a name="input_write_metrics_access_policy_name"></a> [write\_metrics\_access\_policy\_name](#input\_write\_metrics\_access\_policy\_name) | Write metrics access policy name | `string` | `"metrics-write"` | no |
| <a name="input_write_metrics_access_policy_token_name"></a> [write\_metrics\_access\_policy\_token\_name](#input\_write\_metrics\_access\_policy\_token\_name) | Write metrics access policy token name | `string` | `""` | no |
| <a name="input_write_traces_access_policy_display_name"></a> [write\_traces\_access\_policy\_display\_name](#input\_write\_traces\_access\_policy\_display\_name) | Write traces access policy display name | `string` | `""` | no |
| <a name="input_write_traces_access_policy_extra_scopes"></a> [write\_traces\_access\_policy\_extra\_scopes](#input\_write\_traces\_access\_policy\_extra\_scopes) | Extra scopes to add to the write traces access policy | `list(string)` | `[]` | no |
| <a name="input_write_traces_access_policy_name"></a> [write\_traces\_access\_policy\_name](#input\_write\_traces\_access\_policy\_name) | Write traces access policy name | `string` | `"traces-write"` | no |
| <a name="input_write_traces_access_policy_token_name"></a> [write\_traces\_access\_policy\_token\_name](#input\_write\_traces\_access\_policy\_token\_name) | Write traces access policy token name | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_logs_read_user_credentials"></a> [logs\_read\_user\_credentials](#output\_logs\_read\_user\_credentials) | n/a |
| <a name="output_logs_write_user_credentials"></a> [logs\_write\_user\_credentials](#output\_logs\_write\_user\_credentials) | n/a |
| <a name="output_metrics_read_user_credentials"></a> [metrics\_read\_user\_credentials](#output\_metrics\_read\_user\_credentials) | n/a |
| <a name="output_metrics_write_user_credentials"></a> [metrics\_write\_user\_credentials](#output\_metrics\_write\_user\_credentials) | n/a |
| <a name="output_stack_service_account_key"></a> [stack\_service\_account\_key](#output\_stack\_service\_account\_key) | n/a |
| <a name="output_stack_slug"></a> [stack\_slug](#output\_stack\_slug) | Stack slug attribute |
| <a name="output_stack_url"></a> [stack\_url](#output\_stack\_url) | Stack url |
| <a name="output_traces_read_user_credentials"></a> [traces\_read\_user\_credentials](#output\_traces\_read\_user\_credentials) | n/a |
| <a name="output_traces_write_user_credentials"></a> [traces\_write\_user\_credentials](#output\_traces\_write\_user\_credentials) | n/a |
<!-- END_TF_DOCS -->