variable "git_repository" {
  type        = string
  default     = ""
  description = "Terraform git repository"
}

variable "project_owner" {
  type        = string
  default     = ""
  description = "Project owner name"
}

variable "extra_tags" {
  type        = map(string)
  description = "Extra tags for all the resources"
  default     = {}
}

###################
# Certificates
###################
variable "ca_file_path" {
  type        = string
  description = "Path to the CA certificate"
}

variable "server_crt_file_path" {
  type        = string
  description = "Path to server certificate"
}

variable "server_private_crt_file_path" {
  type        = string
  description = "Path to private server certificate"
}

###################
# Networking
###################
variable "client_cidr_block" {
  type        = string
  description = "Network CIDR to use for clients"
  default     = "10.1.0.0/22"
}

variable "vpc_id" {
  type        = string
  description = "ID of VPC to attach VPN to"
}

variable "vpc_subnets" {
  type        = map(string)
  description = "List of subnets to associate with the VPN endpoint"
}
