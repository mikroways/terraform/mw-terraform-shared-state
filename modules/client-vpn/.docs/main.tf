module "vpn" {
  source = "gitlab.com/mikroways/client-vpn/aws"

  ca_file_path                 = "${path.module}/certificates/ca.crt"
  server_crt_file_path         = "${path.module}/certificates/issued/server.crt"
  server_private_crt_file_path = "${path.module}/certificates/private/server.key"
  vpc_id                       = "vpc-xxxxxxxxxxxxxxxxx"
  vpc_subnets                  = [
    "subnet-xxxxxxxxxxxxxxxxx"
  ]
}
