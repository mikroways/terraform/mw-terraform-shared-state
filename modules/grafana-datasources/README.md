# Grafana datasources module

<!-- BEGIN_TF_DOCS -->
## Usage

```hcl
module "example" {
  source = "gitlab.com/mikroways/grafana-datasources/grafana"

  client_name                     = "example"
  configure_datasource_conections = true

  metrics_create   = true
  metrics_username = module.stack_module_example.metrics_read_user_credentials.username
  metrics_password = module.stack_module_example.metrics_read_user_credentials.password

  logs_create   = true
  logs_username = module.stack_module_example.logs_read_user_credentials.username
  logs_password = module.stack_module_example.logs_read_user_credentials.password

  traces_create   = true
  traces_username = module.stack_module_example.traces_read_user_credentials.username
  traces_password = module.stack_module_example.traces_read_user_credentials.password
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.8.2 |
| <a name="requirement_grafana"></a> [grafana](#requirement\_grafana) | 2.18.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.6.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_grafana"></a> [grafana](#provider\_grafana) | 2.18.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.6.2 |

## Resources

| Name | Type |
|------|------|
| [grafana_data_source.logs](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/data_source) | resource |
| [grafana_data_source.metrics](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/data_source) | resource |
| [grafana_data_source.traces](https://registry.terraform.io/providers/grafana/grafana/2.18.0/docs/resources/data_source) | resource |
| [random_uuid.logs](https://registry.terraform.io/providers/hashicorp/random/3.6.2/docs/resources/uuid) | resource |
| [random_uuid.metrics](https://registry.terraform.io/providers/hashicorp/random/3.6.2/docs/resources/uuid) | resource |
| [random_uuid.traces](https://registry.terraform.io/providers/hashicorp/random/3.6.2/docs/resources/uuid) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_client_name"></a> [client\_name](#input\_client\_name) | Client name used as prefix in all the datasources | `string` | n/a | yes |
| <a name="input_configure_datasource_conections"></a> [configure\_datasource\_conections](#input\_configure\_datasource\_conections) | Configure the connection between the created datasouces, such as traces to logs, logs to traces and metrics (exemplars) to traces | `bool` | `false` | no |
| <a name="input_logs_create"></a> [logs\_create](#input\_logs\_create) | Whether to create the logs datasource | `bool` | `false` | no |
| <a name="input_logs_extra_json_data"></a> [logs\_extra\_json\_data](#input\_logs\_extra\_json\_data) | Extra json data encoded to add in logs data source | `map(any)` | `{}` | no |
| <a name="input_logs_extra_secure_json_data"></a> [logs\_extra\_secure\_json\_data](#input\_logs\_extra\_secure\_json\_data) | Extra secure json data encoded to add in logs data source | `map(any)` | `{}` | no |
| <a name="input_logs_password"></a> [logs\_password](#input\_logs\_password) | Logs datasource password | `string` | `""` | no |
| <a name="input_logs_trace_id_matcher_type"></a> [logs\_trace\_id\_matcher\_type](#input\_logs\_trace\_id\_matcher\_type) | Matcher used to extract the trace id from the log | `string` | `"regex"` | no |
| <a name="input_logs_trace_id_regex"></a> [logs\_trace\_id\_regex](#input\_logs\_trace\_id\_regex) | Regex used to retrieve the trace id from the log | `string` | `".*\\\"trace_id\\\":\\\"(\\w+)\\\".*"` | no |
| <a name="input_logs_url"></a> [logs\_url](#input\_logs\_url) | Logs url for the datasource | `string` | `"https://logs-prod-006.grafana.net"` | no |
| <a name="input_logs_username"></a> [logs\_username](#input\_logs\_username) | Logs datasource username | `string` | `""` | no |
| <a name="input_metrics_create"></a> [metrics\_create](#input\_metrics\_create) | Whether to create the metrics datasource | `bool` | `false` | no |
| <a name="input_metrics_exemplars_trace_id_field"></a> [metrics\_exemplars\_trace\_id\_field](#input\_metrics\_exemplars\_trace\_id\_field) | Field in the exemplars with the trace id | `string` | `"trace_id"` | no |
| <a name="input_metrics_extra_json_data"></a> [metrics\_extra\_json\_data](#input\_metrics\_extra\_json\_data) | Extra json data encoded to add in metrics data source | `map(any)` | `{}` | no |
| <a name="input_metrics_extra_secure_json_data"></a> [metrics\_extra\_secure\_json\_data](#input\_metrics\_extra\_secure\_json\_data) | Extra secure json data encoded to add in metrics data source | `map(any)` | `{}` | no |
| <a name="input_metrics_password"></a> [metrics\_password](#input\_metrics\_password) | Metrics datasource password | `string` | `""` | no |
| <a name="input_metrics_url"></a> [metrics\_url](#input\_metrics\_url) | Metrics url for the datasource | `string` | `"https://prometheus-prod-13-prod-us-east-0.grafana.net/api/prom"` | no |
| <a name="input_metrics_username"></a> [metrics\_username](#input\_metrics\_username) | Metrics datasource username | `string` | `""` | no |
| <a name="input_traces_create"></a> [traces\_create](#input\_traces\_create) | Whether to create the traces datasource | `bool` | `false` | no |
| <a name="input_traces_extra_json_data"></a> [traces\_extra\_json\_data](#input\_traces\_extra\_json\_data) | Extra json data encoded to add in traces data source | `map(any)` | `{}` | no |
| <a name="input_traces_extra_secure_json_data"></a> [traces\_extra\_secure\_json\_data](#input\_traces\_extra\_secure\_json\_data) | Extra secure json data encoded to add in traces data source | `map(any)` | `{}` | no |
| <a name="input_traces_log_tag"></a> [traces\_log\_tag](#input\_traces\_log\_tag) | Identifier of the service in the logs. It could be used in the variable traces\_trace\_id\_query as '{${\_\_tags}}' | `string` | `"service_name"` | no |
| <a name="input_traces_metrics_tag"></a> [traces\_metrics\_tag](#input\_traces\_metrics\_tag) | Identifier of the service in the metrics | `string` | `"service_name"` | no |
| <a name="input_traces_password"></a> [traces\_password](#input\_traces\_password) | Traces datasource password | `string` | `""` | no |
| <a name="input_traces_trace_id_query"></a> [traces\_trace\_id\_query](#input\_traces\_trace\_id\_query) | Query used to retrieve logs using the trace id | `string` | `"{${__tags}} | json | attributes_trace_id=\"${__trace.traceId}\""` | no |
| <a name="input_traces_trace_tag"></a> [traces\_trace\_tag](#input\_traces\_trace\_tag) | Identifier of the service in the traces. It could be used in the variable traces\_trace\_id\_query as '{${\_\_tags}}' | `string` | `"service.name"` | no |
| <a name="input_traces_url"></a> [traces\_url](#input\_traces\_url) | Traces url for the datasource | `string` | `"https://tempo-prod-04-prod-us-east-0.grafana.net/tempo"` | no |
| <a name="input_traces_username"></a> [traces\_username](#input\_traces\_username) | Traces datasource username | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_logs_datasource_uid"></a> [logs\_datasource\_uid](#output\_logs\_datasource\_uid) | Logs datasource identifier |
| <a name="output_metrics_datasource_uid"></a> [metrics\_datasource\_uid](#output\_metrics\_datasource\_uid) | Metrics datasource identifier |
| <a name="output_traces_datasource_uid"></a> [traces\_datasource\_uid](#output\_traces\_datasource\_uid) | Traces datasource identifier |
<!-- END_TF_DOCS -->
