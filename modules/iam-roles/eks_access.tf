#########
# Policy
#########
data "aws_iam_policy_document" "eks_access" {
  count = var.eks_access_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "eks:DescribeCluster",
      "eks:ListClusters",
      "eks:AccessKubernetesApi",
      "kms:Decrypt",
      "kms:Encrypt"
    ]
  }
}

resource "aws_iam_policy" "eks_access" {
  count       = var.eks_access_policy ? 1 : 0
  name        = "${var.role_prefix}EKSAccess"
  path        = "/"
  description = "Least privilege policy required to interact with an EKS cluster"
  policy      = data.aws_iam_policy_document.eks_access[0].json
}

###############
# Account role
###############

resource "aws_iam_role" "eks_access" {
  count = local.create_eks_access_role_role ? 1 : 0
  name  = "${var.role_prefix}EKSAccess"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"

    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringLike = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.eks_access_role.repo_paths, var.eks_access_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.eks_access_role.include_pull_request ? [
                  for path in var.eks_access_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}EKSAccess"
  }
}

resource "aws_iam_role_policy_attachment" "eks_access_role_attach" {
  count      = local.create_eks_access_role_role ? 1 : 0
  role       = aws_iam_role.eks_access[0].name
  policy_arn = aws_iam_policy.eks_access[0].arn
}

