module "example" {
  source = "gitlab.com/mikroways/grafana-datasources/grafana"

  client_name                     = "example"
  configure_datasource_conections = true

  metrics_create   = true
  metrics_username = module.stack_module_example.metrics_read_user_credentials.username
  metrics_password = module.stack_module_example.metrics_read_user_credentials.password

  logs_create   = true
  logs_username = module.stack_module_example.logs_read_user_credentials.username
  logs_password = module.stack_module_example.logs_read_user_credentials.password

  traces_create   = true
  traces_username = module.stack_module_example.traces_read_user_credentials.username
  traces_password = module.stack_module_example.traces_read_user_credentials.password
}
