##########
# Policy #
##########

data "aws_iam_policy_document" "extra_services" {
  count = var.extra_services_policy && var.extra_services_policy_actions != [] ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions   = var.extra_services_policy_actions
  }
}

resource "aws_iam_policy" "extra_services" {
  count       = var.extra_services_policy && var.extra_services_policy_actions != [] ? 1 : 0
  name        = "${var.role_prefix}ExtraServices"
  path        = "/"
  description = "Least privilege policy required to create required IAM Roles and Policies"
  policy      = data.aws_iam_policy_document.extra_services[0].json
}

################
# Account role #
################

resource "aws_iam_role" "extra_services" {
  count = local.create_extra_services_role ? 1 : 0
  name  = "${var.role_prefix}ExtraServices"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"

    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringLike = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.extra_services_role.repo_paths, var.extra_services_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.extra_services_role.include_pull_request ? [
                  for path in var.extra_services_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}ExtraServices"
  }
}

resource "aws_iam_role_policy_attachment" "extra_services_role_attach" {
  count      = local.create_extra_services_role ? 1 : 0
  role       = aws_iam_role.extra_services[0].name
  policy_arn = aws_iam_policy.extra_services[0].arn
}

