#################
# General config
#################
variable "client_name" {
  type        = string
  description = "Client name used as stack name and access policies prefix"
}

variable "client_name_slug" {
  type        = string
  default     = ""
  description = "Client name used as stack slug"
}

variable "region" {
  type        = string
  default     = "prod-us-east-0"
  description = "Stack region"
}

variable "access_policy_region" {
  type        = string
  default     = ""
  description = "Override the Access Policies region slug"
}

variable "access_policy_realm" {
  type        = string
  default     = "stack"
  description = "Define the access policy realm type"
  validation {
    condition     = contains(["stack", "org"], var.access_policy_realm)
    error_message = "expected one of 'stack' or 'org'"
  }
}

variable "access_policy_metrics_label_policy" {
  type        = list(string)
  default     = []
  description = "Define extra label policies for the metrics access policies"
}

variable "access_policy_logs_label_policy" {
  type        = list(string)
  default     = []
  description = "Define extra label policies for the logs access policies"
}

variable "access_policy_traces_label_policy" {
  type        = list(string)
  default     = []
  description = "Define extra label policies for the traces access policies"
}

##################
# Service account
##################
variable "create_stack_service_account" {
  type        = bool
  default     = false
  description = "Whether to create a stack service account for terraform"
}

#######################
# Read access policies
#######################
variable "create_read_access_policies" {
  type        = bool
  default     = false
  description = "Whether to create each access policy with scope metrics:read, logs:read and traces:read"
}

variable "create_read_metrics_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope metrics:read"
}

variable "read_metrics_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the read metrics access policy"
}

variable "read_metrics_access_policy_name" {
  type        = string
  default     = "metrics-read"
  description = "Read metrics access policy name"
}

variable "read_metrics_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Read metrics access policy display name"
}

variable "read_metrics_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Read metrics access policy token name"
}

variable "create_read_logs_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope logs:read"
}

variable "read_logs_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the read logs access policy"
}

variable "read_logs_access_policy_name" {
  type        = string
  default     = "logs-read"
  description = "Read logs access policy name"
}

variable "read_logs_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Read logs access policy display name"
}

variable "read_logs_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Read logs access policy token name"
}

variable "create_read_traces_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope traces:read"
}

variable "read_traces_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the read traces access policy"
}

variable "read_traces_access_policy_name" {
  type        = string
  default     = "traces-read"
  description = "Read traces access policy name"
}

variable "read_traces_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Read traces access policy display name"
}

variable "read_traces_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Read logs access policy token name"
}

#######################
# Read access policies
#######################
variable "create_write_access_policies" {
  type        = bool
  default     = false
  description = "Whether to create each access policy with scope metrics:write, logs:write and traces:write"
}

variable "create_write_metrics_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope metrics:write"
}

variable "write_metrics_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the write metrics access policy"
}

variable "write_metrics_access_policy_name" {
  type        = string
  default     = "metrics-write"
  description = "Write metrics access policy name"
}

variable "write_metrics_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Write metrics access policy display name"
}

variable "write_metrics_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Write metrics access policy token name"
}

variable "create_write_logs_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope logs:write"
}

variable "write_logs_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the write logs access policy"
}

variable "write_logs_access_policy_name" {
  type        = string
  default     = "logs-write"
  description = "Write logs access policy name"
}

variable "write_logs_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Write logs access policy display name"
}

variable "write_logs_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Write logs access policy token name"
}

variable "create_write_traces_access_policy" {
  type        = bool
  default     = false
  description = "Whether to create the access policy with scope traces:write"
}

variable "write_traces_access_policy_extra_scopes" {
  type        = list(string)
  default     = []
  description = "Extra scopes to add to the write traces access policy"
}

variable "write_traces_access_policy_name" {
  type        = string
  default     = "traces-write"
  description = "Write traces access policy name"
}

variable "write_traces_access_policy_display_name" {
  type        = string
  default     = ""
  description = "Write traces access policy display name"
}

variable "write_traces_access_policy_token_name" {
  type        = string
  default     = ""
  description = "Write traces access policy token name"
}
