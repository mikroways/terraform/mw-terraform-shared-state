#########
# Policy
#########
data "aws_iam_policy_document" "network_management" {
  count = var.network_management_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ec2:AllocateAddress",
      "ec2:AssociateRouteTable",
      "ec2:AttachInternetGateway",
      "ec2:CreateInternetGateway",
      "ec2:CreateNatGateway",
      "ec2:CreateNetworkAclEntry",
      "ec2:CreateRoute",
      "ec2:CreateRouteTable",
      "ec2:CreateSubnet",
      "ec2:CreateTags",
      "ec2:CreateTransitGatewayVpcAttachment",
      "ec2:CreateVpc",
      "ec2:DeleteInternetGateway",
      "ec2:DeleteNatGateway",
      "ec2:DeleteNetworkAclEntry",
      "ec2:DeleteRoute",
      "ec2:DeleteRouteTable",
      "ec2:DeleteSubnet",
      "ec2:DeleteTags",
      "ec2:DeleteTransitGatewayVpcAttachment",
      "ec2:DeleteVpc",
      "ec2:DescribeAddresses",
      "ec2:DescribeInternetGateways",
      "ec2:DescribeNatGateways",
      "ec2:DescribeNetworkAcls",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroupRules",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeTags",
      "ec2:DescribeTransitGateways",
      "ec2:DescribeTransitGatewayVpcAttachments",
      "ec2:DescribeVpcAttribute",
      "ec2:DescribeVpcs",
      "ec2:DetachInternetGateway",
      "ec2:DisassociateAddress",
      "ec2:DisassociateRouteTable",
      "ec2:ModifySubnetAttribute",
      "ec2:ModifyTransitGatewayVpcAttachment",
      "ec2:ModifyVpcAttribute",
      "ec2:ReleaseAddress",
      "ec2:ReplaceRoute",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress",
      "rds:DescribeDBSubnetGroups",
      "ec2:DescribeAddressesAttribute",
      "ec2:CreateSecurityGroup",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
      "ec2:UpdateSecurityGroupRuleDescriptionsEgress",
      "ec2:DeleteSecurityGroup"
    ]
  }
  dynamic "statement" {
    for_each = var.network_management_extra_actions
    content {
      sid       = "Statement${title(statement.key)}"
      effect    = "Allow"
      resources = statement.value.resources
      actions   = statement.value.actions
    }
  }
}

resource "aws_iam_policy" "network_management" {
  count       = var.network_management_policy ? 1 : 0
  name        = "${var.role_prefix}NetworkManagement"
  path        = "/"
  description = "Least privilege policy required to create network resources in an AWS account"
  policy      = data.aws_iam_policy_document.network_management[0].json
}

###############
# Account role
###############
resource "aws_iam_role" "network_management" {
  count = local.create_network_management_role ? 1 : 0
  name  = "${var.role_prefix}NetworkManagement"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringEquals = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.network_management_role.repo_paths, var.network_management_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.network_management_role.include_pull_request ? [
                  for path in var.network_management_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}NetworkManagement"
  }
}

resource "aws_iam_role_policy_attachment" "network_management_role_attach" {
  count      = local.create_network_management_role ? 1 : 0
  role       = aws_iam_role.network_management[0].name
  policy_arn = aws_iam_policy.network_management[0].arn
}
