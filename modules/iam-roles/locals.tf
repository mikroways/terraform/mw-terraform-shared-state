locals {
  # Role create conditions
  create_network_management_role = (
    var.network_management_policy &&
    var.network_management_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_eks_access_role_role = (
    var.eks_access_policy &&
    var.eks_access_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_eks_management_role = (
    var.eks_management_policy &&
    var.eks_management_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_extra_services_role = (
    var.extra_services_policy &&
    var.extra_services_role != null &&
    var.extra_services_policy_actions != [] &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_kms_access_role = (
    var.kms_policy &&
    var.kms_access_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_kms_management_role = (
    var.kms_policy &&
    var.kms_management_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_roles_management_role = (
    var.roles_management_policy &&
    var.roles_management_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  create_route53_management_role = (
    var.route53_management_policy &&
    var.route53_management_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )

  create_shared_state_role = (
    var.shared_state_policy &&
    var.shared_state_role != null &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )

  identity_provider_default = {
    github = {
      host           = "token.actions.githubusercontent.com"
      subject_prefix = "repo"
      subject_ref    = "ref:refs/heads/"
    }
    gitlab = {
      host           = "gitlab.com"
      subject_prefix = "project_path"
      subject_ref    = "ref_type:branch:ref:"
    }
  }

  identity_providers = {
    for name, identity_provider in var.identity_providers :
    name => {
      type           = identity_provider.type
      host           = identity_provider.host != null ? identity_provider.host : local.identity_provider_default[identity_provider.type].host
      arn            = identity_provider.arn != null ? identity_provider.arn : data.aws_iam_openid_connect_provider.this[name].arn
      subject_prefix = local.identity_provider_default[identity_provider.type].subject_prefix
      subject_ref    = local.identity_provider_default[identity_provider.type].subject_ref
      audience       = identity_provider.audience != null ? identity_provider.audience : var.audience
    }
  }

  assume_role_from_accounts = [
    for account in var.assume_role_from_accounts :
    {
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        AWS = "arn:aws:iam::${account}:root"
      }
    }
  ]
}
