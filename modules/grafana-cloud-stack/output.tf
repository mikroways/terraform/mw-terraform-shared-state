output "stack_slug" {
  description = "Stack slug attribute"
  value       = grafana_cloud_stack.this.slug
}

output "stack_url" {
  description = "Stack url"
  value       = grafana_cloud_stack.this.url
}

output "metrics_read_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.prometheus_url
    username = grafana_cloud_stack.this.prometheus_user_id
    password = try(grafana_cloud_access_policy_token.metrics_read[0].token, null)
  }
}

output "logs_read_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.logs_url
    username = grafana_cloud_stack.this.logs_user_id
    password = try(grafana_cloud_access_policy_token.logs_read[0].token, null)
  }
}

output "traces_read_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.traces_url
    username = grafana_cloud_stack.this.traces_user_id
    password = try(grafana_cloud_access_policy_token.traces_read[0].token, null)
  }
}

output "metrics_write_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.prometheus_url
    username = grafana_cloud_stack.this.prometheus_user_id
    password = try(grafana_cloud_access_policy_token.metrics_write[0].token, null)
  }
}

output "logs_write_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.logs_url
    username = grafana_cloud_stack.this.logs_user_id
    password = try(grafana_cloud_access_policy_token.logs_write[0].token, null)
  }
}

output "traces_write_user_credentials" {
  value = {
    url      = grafana_cloud_stack.this.traces_url
    username = grafana_cloud_stack.this.traces_user_id
    password = try(grafana_cloud_access_policy_token.traces_write[0].token, null)
  }
}

output "stack_service_account_key" {
  value = length(grafana_cloud_stack_service_account_token.terraform) > 0 ? grafana_cloud_stack_service_account_token.terraform[0].key : null
}
