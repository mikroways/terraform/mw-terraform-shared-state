locals {
  client_name_slug = coalesce(var.client_name_slug, var.client_name)
  #######################
  # Read access policies
  #######################
  create_read_metrics_access_policy       = var.create_read_access_policies || var.create_read_metrics_access_policy
  read_metrics_access_policy_display_name = coalesce(var.read_metrics_access_policy_display_name, title(var.read_metrics_access_policy_name))
  read_metrics_access_policy_token_name   = coalesce(var.read_metrics_access_policy_token_name, var.read_metrics_access_policy_name)

  create_read_logs_access_policy       = var.create_read_access_policies || var.create_read_logs_access_policy
  read_logs_access_policy_display_name = coalesce(var.read_logs_access_policy_display_name, title(var.read_logs_access_policy_name))
  read_logs_access_policy_token_name   = coalesce(var.read_logs_access_policy_token_name, var.read_logs_access_policy_name)

  create_read_traces_access_policy       = var.create_read_access_policies || var.create_read_traces_access_policy
  read_traces_access_policy_display_name = coalesce(var.read_traces_access_policy_display_name, title(var.read_traces_access_policy_name))
  read_traces_access_policy_token_name   = coalesce(var.read_traces_access_policy_token_name, var.read_traces_access_policy_name)

  ########################
  # Write access policies
  ########################
  create_write_metrics_access_policy       = var.create_write_access_policies || var.create_write_metrics_access_policy
  write_metrics_access_policy_display_name = coalesce(var.write_metrics_access_policy_display_name, title(var.write_metrics_access_policy_name))
  write_metrics_access_policy_token_name   = coalesce(var.write_metrics_access_policy_token_name, var.write_metrics_access_policy_name)

  create_write_logs_access_policy       = var.create_write_access_policies || var.create_write_logs_access_policy
  write_logs_access_policy_display_name = coalesce(var.write_logs_access_policy_display_name, title(var.write_logs_access_policy_name))
  write_logs_access_policy_token_name   = coalesce(var.write_logs_access_policy_token_name, var.write_logs_access_policy_name)

  create_write_traces_access_policy       = var.create_write_access_policies || var.create_write_traces_access_policy
  write_traces_access_policy_display_name = coalesce(var.write_traces_access_policy_display_name, title(var.write_traces_access_policy_name))
  write_traces_access_policy_token_name   = coalesce(var.write_traces_access_policy_token_name, var.write_traces_access_policy_name)
}
