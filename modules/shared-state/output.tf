output "roles" {
  value = {
    for name, role in aws_iam_role.this :
    name => {
      arn : role.arn
      assume_role_policy : jsondecode(role.assume_role_policy)
    }
  }
}