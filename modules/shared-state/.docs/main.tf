module "terraform-shared-state" {
  source = "gitlab.com/mikroways/shared-state/aws"

  bucket_name                 = "terraform-state-bucket"
  dynamodb_table_name         = "TerraformSharedState"
  git_repository              = "https://gitlab.example.com/project/aws-terraform-shared-state.git"

  identity_providers = {
    gitlab = {
      type = "gitlab"
    }
  }

  environments = [
    {
      name     = "SharedSvcs"
      projects = [
        {
          name                 = "networking"
          git_project_path     = "https://gitlab.example.com/project/aws-networking.git"
          git_project_branches = ["main"]
        },
        {
          name                 = "eks"
          git_project_path     = "https://gitlab.example.com/project/aws-eks.git"
          git_project_branches = ["main"]
        }
      ]
    }
  ]
}
