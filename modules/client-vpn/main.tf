terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.67.0"
    }
    awsutils = {
      source = "cloudposse/awsutils"
      version = "~> 0.19.1"
    }
  }
}

provider "aws" {
  default_tags {
    tags = merge(
      { "Managed-by" = "Terraform" },
      var.git_repository == null ? {} : { "Repository" = var.git_repository },
      var.project_owner == null ? {} : { "Owner" = var.project_owner },
      var.extra_tags,
    )
  }
}