########################
# Write access policies
########################
resource "grafana_cloud_access_policy" "metrics_write" {
  count        = local.create_write_metrics_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.write_metrics_access_policy_name
  display_name = local.write_metrics_access_policy_display_name

  scopes = concat(["metrics:write"], var.write_metrics_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id
    dynamic "label_policy" {
      for_each = var.access_policy_metrics_label_policy
      content {
        selector = label_policy.value
      }
    }
  }
}

resource "grafana_cloud_access_policy_token" "metrics_write" {
  count            = local.create_write_metrics_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.metrics_write[0].policy_id
  name             = local.write_metrics_access_policy_token_name
  display_name     = local.write_metrics_access_policy_display_name
}

resource "grafana_cloud_access_policy" "logs_write" {
  count        = local.create_write_logs_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.write_logs_access_policy_name
  display_name = local.write_logs_access_policy_display_name

  scopes = concat(["logs:write"], var.write_logs_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id
    dynamic "label_policy" {
      for_each = var.access_policy_logs_label_policy
      content {
        selector = label_policy.value
      }
    }
  }
}

resource "grafana_cloud_access_policy_token" "logs_write" {
  count            = local.create_write_logs_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.logs_write[0].policy_id
  name             = local.write_logs_access_policy_token_name
  display_name     = local.write_logs_access_policy_display_name
}

resource "grafana_cloud_access_policy" "traces_write" {
  count        = local.create_write_traces_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.write_traces_access_policy_name
  display_name = local.write_traces_access_policy_display_name

  scopes = concat(["traces:write"], var.write_traces_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id
    dynamic "label_policy" {
      for_each = var.access_policy_traces_label_policy
      content {
        selector = label_policy.value
      }
    }
  }
}

resource "grafana_cloud_access_policy_token" "traces_write" {
  count            = local.create_write_traces_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.traces_write[0].policy_id
  name             = local.write_traces_access_policy_token_name
  display_name     = local.write_traces_access_policy_display_name
}

#######################
# Read access policies
#######################
resource "grafana_cloud_access_policy" "metrics_read" {
  count        = local.create_read_metrics_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.read_metrics_access_policy_name
  display_name = local.read_metrics_access_policy_display_name

  scopes = concat(["metrics:read"], var.read_metrics_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id
    dynamic "label_policy" {
      for_each = var.access_policy_metrics_label_policy
      content {
        selector = label_policy.value
      }
    }
  }

}

resource "grafana_cloud_access_policy_token" "metrics_read" {
  count            = local.create_read_metrics_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.metrics_read[0].policy_id
  name             = local.read_metrics_access_policy_token_name
  display_name     = local.read_metrics_access_policy_display_name
}

resource "grafana_cloud_access_policy" "logs_read" {
  count        = local.create_read_logs_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.read_logs_access_policy_name
  display_name = local.read_logs_access_policy_display_name

  scopes = concat(["logs:read"], var.read_logs_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id
    dynamic "label_policy" {
      for_each = var.access_policy_logs_label_policy
      content {
        selector = label_policy.value
      }
    }
  }
}

resource "grafana_cloud_access_policy_token" "logs_read" {
  count            = local.create_read_logs_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.logs_read[0].policy_id
  name             = local.read_logs_access_policy_token_name
  display_name     = local.read_logs_access_policy_display_name
}

resource "grafana_cloud_access_policy" "traces_read" {
  count        = local.create_read_traces_access_policy ? 1 : 0
  region       = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  name         = var.read_traces_access_policy_name
  display_name = local.read_traces_access_policy_display_name

  scopes = concat(["traces:read"], var.read_traces_access_policy_extra_scopes)

  realm {
    type       = var.access_policy_realm
    identifier = var.access_policy_realm == "stack" ? grafana_cloud_stack.this.id : grafana_cloud_stack.this.org_id

    dynamic "label_policy" {
      for_each = var.access_policy_traces_label_policy
      content {
        selector = label_policy.value
      }
    }
  }
}

resource "grafana_cloud_access_policy_token" "traces_read" {
  count            = local.create_read_traces_access_policy ? 1 : 0
  region           = coalesce(var.access_policy_region, grafana_cloud_stack.this.region_slug)
  access_policy_id = grafana_cloud_access_policy.traces_read[0].policy_id
  name             = local.read_traces_access_policy_token_name
  display_name     = local.read_traces_access_policy_display_name
}
