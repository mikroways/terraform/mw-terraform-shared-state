locals {
  metrics_datasource_uid     = var.configure_datasource_conections ? random_uuid.metrics[0].id : null
  logs_datasource_uid     = var.configure_datasource_conections ? random_uuid.logs[0].id : null
  traces_datasource_uid     = var.configure_datasource_conections ? random_uuid.traces[0].id : null

  traces_to_logs = var.configure_datasource_conections && var.logs_create ? {
    tracesToLogsV2 = {
      customQuery        = true
      datasourceUid      = local.logs_datasource_uid
      query              = var.traces_trace_id_query
      spanEndTimeShift   = "5m"
      spanStartTimeShift = "-5m"
      tags = [
        {
          key   = var.traces_trace_tag
          value = var.traces_log_tag
        }
      ]
    }
  } : {}

  traces_to_metrics = var.configure_datasource_conections && var.metrics_create ? {
    tracesToMetrics = {
      datasourceUid      = local.metrics_datasource_uid
      spanEndTimeShift   = "5m"
      spanStartTimeShift = "-5m"
      tags = [
        {
          key   = var.traces_trace_tag
          value = var.traces_metrics_tag
        }
      ]
    }
  } : {}

  traces_service_map = var.configure_datasource_conections && var.metrics_create ? {
    serviceMap = {
      datasourceUid = local.metrics_datasource_uid
    }
  } : {}

  logs_to_traces = var.configure_datasource_conections && var.traces_create ? {
    derivedFields = [
      {
        datasourceUid   = local.traces_datasource_uid
        matcherRegex    = var.logs_trace_id_regex
        matcherType     = var.logs_trace_id_matcher_type
        name            = "TraceID"
        url             = "$${__value.raw}"
        urlDisplayLabel = "Go to trace"
      }
    ]
  } : {}

  exemplars_to_traces = var.configure_datasource_conections && var.traces_create ? {
    exemplarTraceIdDestinations = [
      {
        datasourceUid = local.traces_datasource_uid
        name          = var.metrics_exemplars_trace_id_field
      }
    ]
  } : {}
}
