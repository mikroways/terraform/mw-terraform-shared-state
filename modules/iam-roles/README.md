# iam-roles

For more information about the published version, you can check the [registry](https://gitlab.com/mikroways/terraform/mw-terraform-modules/-/terraform_module_registry)

## Usage

```hcl
module "iam-roles" {
  source = "gitlab.com/mikroways/iam-roles/aws"

  network_management_policy = true
  network_management_role = {
    repo_paths    = ["org/aws-networking"]
    repo_branches = ["main"]
  }

  eks_access_policy = true
  eks_access_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  kms_policy = true

  eks_management_policy = true
  eks_management_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  roles_management_policy = true
  roles_management_role = {
    repo_paths    = ["org/aws-account-access"]
    repo_branches = ["main"]
  }

  extra_services_policy = true
  extra_services_policy_actions = [
    "ses:sendRawEmail"
  ]
  extra_services_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  identity_providers = {
    github = {
      type = "github"
      arn  = "arn:aws:iam::xxxxxxxxxxxx:oidc-provider/token.actions.githubusercontent.com"
    }
  }
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.50.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.50.0 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.cost_explorer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.kms_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.kms_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.kms_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.eks_access_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.eks_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.extra_services_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.kms_access_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.kms_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.network_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.roles_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.route53_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.cost_explorer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.kms_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_audience"></a> [audience](#input\_audience) | Terraform role trust relationship audience | `string` | `"sts.amazonaws.com"` | no |
| <a name="input_billing_ce_policy"></a> [billing\_ce\_policy](#input\_billing\_ce\_policy) | Whether to create the Billing and Cost Explorer policy | `bool` | `false` | no |
| <a name="input_eks_access_policy"></a> [eks\_access\_policy](#input\_eks\_access\_policy) | Whether to create the EKSAccess policy | `bool` | `false` | no |
| <a name="input_eks_access_role"></a> [eks\_access\_role](#input\_eks\_access\_role) | Repositories and branches to create the EKSAccess sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_eks_management_policy"></a> [eks\_management\_policy](#input\_eks\_management\_policy) | Whether to create the EKSManagement policy | `bool` | `false` | no |
| <a name="input_eks_management_role"></a> [eks\_management\_role](#input\_eks\_management\_role) | Repositories and branches to create the EKSManagement sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_extra_services_policy"></a> [extra\_services\_policy](#input\_extra\_services\_policy) | Whether to create the ExtraServices policy | `bool` | `false` | no |
| <a name="input_extra_services_policy_actions"></a> [extra\_services\_policy\_actions](#input\_extra\_services\_policy\_actions) | Actions to add to the ExtraServices Policy (if set to [] the policy won't be created) | `list(string)` | `[]` | no |
| <a name="input_extra_services_role"></a> [extra\_services\_role](#input\_extra\_services\_role) | Repositories and branches to create the ExtraServices sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_git_repository"></a> [git\_repository](#input\_git\_repository) | Terraform git repository | `string` | `""` | no |
| <a name="input_identity_providers"></a> [identity\_providers](#input\_identity\_providers) | Identity providers used | <pre>map(object({<br>    type     = string<br>    arn      = string<br>    host     = optional(string)<br>    audience = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_kms_access_role"></a> [kms\_access\_role](#input\_kms\_access\_role) | Repositories and branches to create the KMSAccess sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_kms_management_role"></a> [kms\_management\_role](#input\_kms\_management\_role) | Repositories and branches to create the KMSManagement sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_kms_policy"></a> [kms\_policy](#input\_kms\_policy) | Wheter to create the KMS policy | `bool` | `false` | no |
| <a name="input_network_management_policy"></a> [network\_management\_policy](#input\_network\_management\_policy) | Wheter to create the networking policy | `bool` | `false` | no |
| <a name="input_network_management_role"></a> [network\_management\_role](#input\_network\_management\_role) | Repositories and branches to create the NetworkManagement sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_project_owner"></a> [project\_owner](#input\_project\_owner) | Project owner name | `string` | `""` | no |
| <a name="input_role_prefix"></a> [role\_prefix](#input\_role\_prefix) | Prefix that will be added to all resources | `string` | `"MW-"` | no |
| <a name="input_roles_management_policy"></a> [roles\_management\_policy](#input\_roles\_management\_policy) | Whether to create the RolesManagement policy | `bool` | `false` | no |
| <a name="input_roles_management_role"></a> [roles\_management\_role](#input\_roles\_management\_role) | Repositories and branches to create the RolesManagement sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |
| <a name="input_route53_management_policy"></a> [route53\_management\_policy](#input\_route53\_management\_policy) | Wheter to create the Route53 policy | `bool` | `false` | no |
| <a name="input_route53_management_role"></a> [route53\_management\_role](#input\_route53\_management\_role) | Repositories and branches to create the Route53Management sub | <pre>object({<br>    repo_paths    = optional(list(string))<br>    repo_branches = optional(list(string))<br>  })</pre> | `null` | no |

<!-- BEGIN_TF_DOCS -->
# iam-roles

For more information about the published version, you can check the [registry](https://gitlab.com/mikroways/terraform/mw-terraform-modules/-/terraform_module_registry)

## Usage

```hcl
module "iam-roles" {
  source = "gitlab.com/mikroways/iam-roles/aws"

  network_management_policy = true
  network_management_role = {
    repo_paths    = ["org/aws-networking"]
    repo_branches = ["main"]
  }

  eks_access_policy = true
  eks_access_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  kms_policy = true

  eks_management_policy = true
  eks_management_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  roles_management_policy = true
  roles_management_role = {
    repo_paths    = ["org/aws-account-access"]
    repo_branches = ["main"]
  }

  extra_services_policy = true
  extra_services_policy_actions = [
    "ses:sendRawEmail"
  ]
  extra_services_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  identity_providers = {
    github = {
      type = "github"
      arn  = "arn:aws:iam::xxxxxxxxxxxx:oidc-provider/token.actions.githubusercontent.com"
    }
  }
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.50.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.50.0 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.cost_explorer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.extra_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.kms_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.shared_state](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.extra_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.kms_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.kms_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.shared_state](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.eks_access_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.eks_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.extra_roles_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.extra_services_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.kms_access_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.kms_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.network_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.roles_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.route53_management_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.shared_state_role_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_openid_connect_provider.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_openid_connect_provider) | data source |
| [aws_iam_policy_document.cost_explorer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.eks_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.eks_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.extra_roles](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.extra_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.kms_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.network_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.roles_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.route53_management](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.shared_state](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_from_accounts"></a> [assume\_role\_from\_accounts](#input\_assume\_role\_from\_accounts) | List of account to allow to assume roles | `list(string)` | `[]` | no |
| <a name="input_audience"></a> [audience](#input\_audience) | Terraform role trust relationship audience | `string` | `"sts.amazonaws.com"` | no |
| <a name="input_billing_ce_policy"></a> [billing\_ce\_policy](#input\_billing\_ce\_policy) | Whether to create the Billing and Cost Explorer policy | `bool` | `false` | no |
| <a name="input_eks_access_policy"></a> [eks\_access\_policy](#input\_eks\_access\_policy) | Whether to create the EKSAccess policy | `bool` | `false` | no |
| <a name="input_eks_access_role"></a> [eks\_access\_role](#input\_eks\_access\_role) | Repositories and branches to create the EKSAccess sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_eks_management_extra_actions"></a> [eks\_management\_extra\_actions](#input\_eks\_management\_extra\_actions) | Extra statements for the EKS Management role policy | <pre>map(object({<br>    actions   = optional(list(string))<br>    resources = optional(list(string))<br>  }))</pre> | `{}` | no |
| <a name="input_eks_management_policy"></a> [eks\_management\_policy](#input\_eks\_management\_policy) | Whether to create the EKSManagement policy | `bool` | `false` | no |
| <a name="input_eks_management_role"></a> [eks\_management\_role](#input\_eks\_management\_role) | Repositories and branches to create the EKSManagement sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_extra_roles"></a> [extra\_roles](#input\_extra\_roles) | Named extra roles with their respective actions | <pre>map(object({<br>    actions              = list(string)<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  }))</pre> | `null` | no |
| <a name="input_extra_services_policy"></a> [extra\_services\_policy](#input\_extra\_services\_policy) | Whether to create the ExtraServices policy | `bool` | `false` | no |
| <a name="input_extra_services_policy_actions"></a> [extra\_services\_policy\_actions](#input\_extra\_services\_policy\_actions) | Actions to add to the ExtraServices Policy (if set to [] the policy won't be created) | `list(string)` | `[]` | no |
| <a name="input_extra_services_role"></a> [extra\_services\_role](#input\_extra\_services\_role) | Repositories and branches to create the ExtraServices sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_git_repository"></a> [git\_repository](#input\_git\_repository) | Terraform git repository | `string` | `""` | no |
| <a name="input_identity_providers"></a> [identity\_providers](#input\_identity\_providers) | Identity providers used | <pre>map(object({<br>    type     = string<br>    arn      = optional(string)<br>    host     = optional(string)<br>    audience = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_kms_access_role"></a> [kms\_access\_role](#input\_kms\_access\_role) | Repositories and branches to create the KMSAccess sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_kms_management_role"></a> [kms\_management\_role](#input\_kms\_management\_role) | Repositories and branches to create the KMSManagement sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_kms_policy"></a> [kms\_policy](#input\_kms\_policy) | Wheter to create the KMS policy | `bool` | `false` | no |
| <a name="input_network_management_extra_actions"></a> [network\_management\_extra\_actions](#input\_network\_management\_extra\_actions) | Extra statements for the Network Management role policy | <pre>map(object({<br>    actions   = optional(list(string))<br>    resources = optional(list(string))<br>  }))</pre> | `{}` | no |
| <a name="input_network_management_policy"></a> [network\_management\_policy](#input\_network\_management\_policy) | Wheter to create the networking policy | `bool` | `false` | no |
| <a name="input_network_management_role"></a> [network\_management\_role](#input\_network\_management\_role) | Repositories and branches to create the NetworkManagement sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_project_owner"></a> [project\_owner](#input\_project\_owner) | Project owner name | `string` | `""` | no |
| <a name="input_role_prefix"></a> [role\_prefix](#input\_role\_prefix) | Prefix that will be added to all resources | `string` | `"MW-"` | no |
| <a name="input_roles_management_extra_actions"></a> [roles\_management\_extra\_actions](#input\_roles\_management\_extra\_actions) | Extra statements for the Roles Management role policy | <pre>map(object({<br>    actions   = optional(list(string))<br>    resources = optional(list(string))<br>  }))</pre> | `{}` | no |
| <a name="input_roles_management_policy"></a> [roles\_management\_policy](#input\_roles\_management\_policy) | Whether to create the RolesManagement policy | `bool` | `false` | no |
| <a name="input_roles_management_role"></a> [roles\_management\_role](#input\_roles\_management\_role) | Repositories and branches to create the RolesManagement sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_route53_management_extra_actions"></a> [route53\_management\_extra\_actions](#input\_route53\_management\_extra\_actions) | Extra statements for the Route53 Management role policy | <pre>map(object({<br>    actions   = optional(list(string))<br>    resources = optional(list(string))<br>  }))</pre> | `{}` | no |
| <a name="input_route53_management_policy"></a> [route53\_management\_policy](#input\_route53\_management\_policy) | Wheter to create the Route53 policy | `bool` | `false` | no |
| <a name="input_route53_management_role"></a> [route53\_management\_role](#input\_route53\_management\_role) | Repositories and branches to create the Route53Management sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>  })</pre> | `null` | no |
| <a name="input_shared_state_policy"></a> [shared\_state\_policy](#input\_shared\_state\_policy) | Whether to create the SharedState policy | `bool` | `false` | no |
| <a name="input_shared_state_role"></a> [shared\_state\_role](#input\_shared\_state\_role) | Repositories and branches to create the SharedState sub | <pre>object({<br>    repo_paths           = optional(list(string))<br>    repo_branches        = optional(list(string))<br>    include_pull_request = bool<br>    aws_region           = optional(string)<br>    aws_account          = optional(string)<br>    dynamodb_table_name  = optional(string)<br>    s3_bucket_name       = optional(string)<br>    oidc_provider_name   = optional(string)<br>  })</pre> | `null` | no |
<!-- END_TF_DOCS -->