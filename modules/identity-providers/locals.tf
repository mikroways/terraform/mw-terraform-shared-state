locals {
  identity_provider_default = {
    github = {
      host        = "token.actions.githubusercontent.com"
      thumbprints = ["1b511abead59c6ce207077c0bf0e0043b1382612"]
    }
    gitlab = {
      host        = "gitlab.com"
      thumbprints = ["b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a"]
    }
  }

  identity_providers = {
    for name, identity_provider in var.identity_providers :
    name => {
      host        = coalesce(identity_provider.host, local.identity_provider_default[identity_provider.type].host)
      thumbprints = coalesce(identity_provider.thumbprints, local.identity_provider_default[identity_provider.type].thumbprints)
      audiences   = coalesce(identity_provider.audiences, [var.audience])
    }
  }
}
