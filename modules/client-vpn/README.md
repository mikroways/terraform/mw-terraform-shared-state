# client vpn

<!-- BEGIN_TF_DOCS -->
## Usage

```hcl
module "vpn" {
  source = "gitlab.com/mikroways/client-vpn/aws"

  ca_file_path                 = "${path.module}/certificates/ca.crt"
  server_crt_file_path         = "${path.module}/certificates/issued/server.crt"
  server_private_crt_file_path = "${path.module}/certificates/private/server.key"
  vpc_id                       = "vpc-xxxxxxxxxxxxxxxxx"
  vpc_subnets                  = [
    "subnet-xxxxxxxxxxxxxxxxx"
  ]
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.67.0 |
| <a name="requirement_awsutils"></a> [awsutils](#requirement\_awsutils) | ~> 0.19.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.67.0 |
| <a name="provider_awsutils"></a> [awsutils](#provider\_awsutils) | ~> 0.19.1 |

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.vpn_server](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_ec2_client_vpn_authorization_rule.vpn_auth_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_authorization_rule) | resource |
| [aws_ec2_client_vpn_endpoint.vpn](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_endpoint) | resource |
| [aws_ec2_client_vpn_network_association.vpn_subnets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_network_association) | resource |
| [aws_security_group.vpn_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [awsutils_ec2_client_vpn_export_client_config.vpn](https://registry.terraform.io/providers/cloudposse/awsutils/latest/docs/data-sources/ec2_client_vpn_export_client_config) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ca_file_path"></a> [ca\_file\_path](#input\_ca\_file\_path) | Path to the CA certificate | `string` | n/a | yes |
| <a name="input_client_cidr_block"></a> [client\_cidr\_block](#input\_client\_cidr\_block) | Network CIDR to use for clients | `string` | `"10.1.0.0/22"` | no |
| <a name="input_extra_tags"></a> [extra\_tags](#input\_extra\_tags) | Extra tags for all the resources | `map(string)` | `{}` | no |
| <a name="input_git_repository"></a> [git\_repository](#input\_git\_repository) | Terraform git repository | `string` | `""` | no |
| <a name="input_project_owner"></a> [project\_owner](#input\_project\_owner) | Project owner name | `string` | `""` | no |
| <a name="input_server_crt_file_path"></a> [server\_crt\_file\_path](#input\_server\_crt\_file\_path) | Path to server certificate | `string` | n/a | yes |
| <a name="input_server_private_crt_file_path"></a> [server\_private\_crt\_file\_path](#input\_server\_private\_crt\_file\_path) | Path to private server certificate | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of VPC to attach VPN to | `string` | n/a | yes |
| <a name="input_vpc_subnets"></a> [vpc\_subnets](#input\_vpc\_subnets) | List of subnets to associate with the VPN endpoint | `map(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certificate_arn"></a> [certificate\_arn](#output\_certificate\_arn) | n/a |
| <a name="output_client_configuration"></a> [client\_configuration](#output\_client\_configuration) | VPN Client Configuration data. |
| <a name="output_sg_id"></a> [sg\_id](#output\_sg\_id) | n/a |
| <a name="output_vpn_endpoint_arn"></a> [vpn\_endpoint\_arn](#output\_vpn\_endpoint\_arn) | The ARN of the Client VPN Endpoint Connection. |
| <a name="output_vpn_endpoint_dns_name"></a> [vpn\_endpoint\_dns\_name](#output\_vpn\_endpoint\_dns\_name) | The DNS Name of the Client VPN Endpoint Connection. |
| <a name="output_vpn_endpoint_id"></a> [vpn\_endpoint\_id](#output\_vpn\_endpoint\_id) | The ID of the Client VPN Endpoint Connection. |
<!-- END_TF_DOCS -->