resource "grafana_cloud_stack" "this" {
  name        = var.client_name
  slug        = local.client_name_slug
  region_slug = var.region
}
