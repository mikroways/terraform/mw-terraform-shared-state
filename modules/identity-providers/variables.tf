variable "git_repository" {
  type        = string
  default     = ""
  description = "Terraform git repository"
}

variable "project_owner" {
  type        = string
  default     = ""
  description = "Project owner name"
}

###################
# OIDC Providers
###################

variable "identity_providers" {
  type = map(object({
    type        = string
    host        = optional(string)
    thumbprints = optional(list(string))
    audiences   = optional(list(string))
  }))
  description = "Map of identity providers to set in the trust relationship"

  validation {
    condition     = alltrue([for id in var.identity_providers : contains(["github", "gitlab"], id.type)])
    error_message = "Valid values for identity_provider_type: 'github' or 'gitlab'"
  }
}

variable "audience" {
  type    = string
  default = "sts.amazonaws.com"
}

