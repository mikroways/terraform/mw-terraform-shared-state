locals {
  create_roles = (
    var.create_roles &&
    (
      local.identity_providers != {} ||
      local.assume_role_from_accounts != []
    )
  )
  identity_provider_default = {
    github = {
      host           = "token.actions.githubusercontent.com"
      subject_prefix = "repo"
      # <PATH>:ref:refs/heads/<branch>
      # <PATH>:pull_request
      subject_ref = "ref:refs/heads/"
    }
    gitlab = {
      host           = "gitlab.com"
      subject_prefix = "project_path"
      # <PATH>:ref_type:branch:ref:<branch>
      subject_ref = "ref_type:branch:ref:"
    }
  }

  identity_providers = {
    for name, identity_provider in var.identity_providers :
    name => {
      type           = identity_provider.type
      arn            = identity_provider.arn != null ? identity_provider.arn : data.aws_iam_openid_connect_provider.this[name].arn
      host           = coalesce(identity_provider.host, local.identity_provider_default[identity_provider.type].host)
      subject_prefix = local.identity_provider_default[identity_provider.type].subject_prefix
      subject_ref    = local.identity_provider_default[identity_provider.type].subject_ref
      audience       = coalesce(identity_provider.audience, var.audience)
    }
  }

  environment = flatten([
    for environment in var.environments : [
      for project in environment.projects : {
        name     = environment.name
        audience = environment.audience
        project  = project.include_pull_request != null ? project : merge(project, { include_pull_request = false })
      }
    ]
  ])

  environment_projects = {
    for environment in local.environment : "${environment.name}.${environment.project.name}" => environment
  }

  assume_role_from_accounts = [
    for account in var.assume_role_from_accounts :
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${account}:root"
        }
      }
  ]
}
