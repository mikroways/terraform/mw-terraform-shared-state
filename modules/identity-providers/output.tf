output "arns" {
  value = {
    for name, identity_provider in local.identity_providers :
    name => aws_iam_openid_connect_provider.default[name].arn
  }
}