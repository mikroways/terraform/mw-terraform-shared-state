#################
# General config
#################
variable "client_name" {
  type        = string
  description = "Client name used as prefix in all the datasources"
}

variable "configure_datasource_conections" {
  type        = bool
  default     = false
  description = "Configure the connection between the created datasouces, such as traces to logs, logs to traces and metrics (exemplars) to traces"
}

#################
# Metrics config
#################
variable "metrics_create" {
  type        = bool
  default     = false
  description = "Whether to create the metrics datasource"
}

variable "metrics_url" {
  type        = string
  default     = "https://prometheus-prod-13-prod-us-east-0.grafana.net/api/prom"
  description = "Metrics url for the datasource"
}

variable "metrics_username" {
  type        = string
  default     = ""
  description = "Metrics datasource username"
}

variable "metrics_password" {
  type        = string
  default     = ""
  description = "Metrics datasource password"
}

variable "metrics_exemplars_trace_id_field" {
  type        = string
  default     = "trace_id"
  description = "Field in the exemplars with the trace id"
}

variable "metrics_extra_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra json data encoded to add in metrics data source"
}

variable "metrics_extra_secure_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra secure json data encoded to add in metrics data source"
}

##############
# Logs config
##############
variable "logs_create" {
  type        = bool
  default     = false
  description = "Whether to create the logs datasource"
}

variable "logs_url" {
  type        = string
  default     = "https://logs-prod-006.grafana.net"
  description = "Logs url for the datasource"
}

variable "logs_username" {
  type        = string
  default     = ""
  description = "Logs datasource username"
}

variable "logs_password" {
  type        = string
  default     = ""
  description = "Logs datasource password"
}

variable "logs_trace_id_regex" {
  type        = string
  default     = ".*\\\"trace_id\\\":\\\"(\\w+)\\\".*"
  description = "Regex used to retrieve the trace id from the log"
}

variable "logs_trace_id_matcher_type" {
  type        = string
  default     = "regex"
  description = "Matcher used to extract the trace id from the log"
  validation {
    condition     = contains(["regex", "label"], var.logs_trace_id_matcher_type)
    error_message = "Valid values for matcher type are (regex, label)."
  } 
}

variable "logs_extra_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra json data encoded to add in logs data source"
}

variable "logs_extra_secure_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra secure json data encoded to add in logs data source"
}

################
# Traces config
################
variable "traces_create" {
  type        = bool
  default     = false
  description = "Whether to create the traces datasource"
}

variable "traces_url" {
  type        = string
  default     = "https://tempo-prod-04-prod-us-east-0.grafana.net/tempo"
  description = "Traces url for the datasource"
}

variable "traces_username" {
  type        = string
  default     = ""
  description = "Traces datasource username"
}

variable "traces_password" {
  type        = string
  default     = ""
  description = "Traces datasource password"
}

variable "traces_trace_id_query" {
  type        = string
  default     = "{$${__tags}} | json | attributes_trace_id=\"$${__trace.traceId}\""
  description = "Query used to retrieve logs using the trace id"
}

variable "traces_trace_tag" {
  type        = string
  default     = "service.name"
  description = "Identifier of the service in the traces. It could be used in the variable traces_trace_id_query as '{$${__tags}}'"
}

variable "traces_log_tag" {
  type        = string
  default     = "service_name"
  description = "Identifier of the service in the logs. It could be used in the variable traces_trace_id_query as '{$${__tags}}'"
}

variable "traces_metrics_tag" {
  type        = string
  default     = "service_name"
  description = "Identifier of the service in the metrics"
}

variable "traces_extra_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra json data encoded to add in traces data source"
}

variable "traces_extra_secure_json_data" {
  type        = map(any)
  default     = {}
  description = "Extra secure json data encoded to add in traces data source"
}