resource "grafana_cloud_stack_service_account" "terraform" {
  count       = var.create_stack_service_account ? 1 : 0
  stack_slug  = grafana_cloud_stack.this.slug
  name        = "terraform"
  role        = "Admin"
  is_disabled = false
}

resource "grafana_cloud_stack_service_account_token" "terraform" {
  count              = var.create_stack_service_account ? 1 : 0
  stack_slug         = grafana_cloud_stack.this.slug
  name               = "terraform"
  service_account_id = grafana_cloud_stack_service_account.terraform[0].id
}
