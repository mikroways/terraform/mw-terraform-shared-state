# shared-state

For more information about the published version, you can check the [registry](https://gitlab.com/mikroways/terraform/mw-terraform-modules/-/terraform_module_registry)

<!-- BEGIN_TF_DOCS -->
## Usage

```hcl
module "terraform-shared-state" {
  source = "gitlab.com/mikroways/shared-state/aws"

  bucket_name                 = "terraform-state-bucket"
  dynamodb_table_name         = "TerraformSharedState"
  git_repository              = "https://gitlab.example.com/project/aws-terraform-shared-state.git"

  identity_providers = {
    gitlab = {
      type = "gitlab"
    }
  }

  environments = [
    {
      name     = "SharedSvcs"
      projects = [
        {
          name                 = "networking"
          git_project_path     = "https://gitlab.example.com/project/aws-networking.git"
          git_project_branches = ["main"]
        },
        {
          name                 = "eks"
          git_project_path     = "https://gitlab.example.com/project/aws-eks.git"
          git_project_branches = ["main"]
        }
      ]
    }
  ]
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.50.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.50.0 |

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.wildcard](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_resourcegroups_group.terraform_infraestructure](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/resourcegroups_group) | resource |
| [aws_s3_bucket.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_iam_openid_connect_provider.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_openid_connect_provider) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_from_accounts"></a> [assume\_role\_from\_accounts](#input\_assume\_role\_from\_accounts) | List of account to allow to assume roles | `list(string)` | `[]` | no |
| <a name="input_audience"></a> [audience](#input\_audience) | Terraform role trust relationship audience | `string` | `"sts.amazonaws.com"` | no |
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Bucket name for the terraform state | `string` | n/a | yes |
| <a name="input_create_policy_wildcard_per_environment"></a> [create\_policy\_wildcard\_per\_environment](#input\_create\_policy\_wildcard\_per\_environment) | Whether to create a policy wildcard per environment | `bool` | `true` | no |
| <a name="input_create_roles"></a> [create\_roles](#input\_create\_roles) | Whether to create a role | `bool` | `true` | no |
| <a name="input_dynamodb_read_capacity"></a> [dynamodb\_read\_capacity](#input\_dynamodb\_read\_capacity) | DynamoDB read capacity. Only used if the billing\_mode is PROVISIONED | `string` | `1` | no |
| <a name="input_dynamodb_table_billing_mode"></a> [dynamodb\_table\_billing\_mode](#input\_dynamodb\_table\_billing\_mode) | DynamoDB table billing mode. Valid options 'PAY\_PER\_REQUEST' and 'PROVISIONED' | `string` | `"PAY_PER_REQUEST"` | no |
| <a name="input_dynamodb_table_name"></a> [dynamodb\_table\_name](#input\_dynamodb\_table\_name) | DynamoDB table name for the terraform state lock | `string` | n/a | yes |
| <a name="input_dynamodb_write_capacity"></a> [dynamodb\_write\_capacity](#input\_dynamodb\_write\_capacity) | DynamoDB write capacity. Only used if the billing\_mode is PROVISIONED | `string` | `1` | no |
| <a name="input_environments"></a> [environments](#input\_environments) | Account list | <pre>list(<br>    object({<br>      name     = string<br>      audience = optional(string)<br>      projects = list(object({<br>        name                 = string<br>        git_project_path     = string<br>        git_project_branches = list(string)<br>        include_pull_request = optional(bool)<br>      }))<br>    })<br>  )</pre> | n/a | yes |
| <a name="input_git_repository"></a> [git\_repository](#input\_git\_repository) | Terraform git repository | `string` | `""` | no |
| <a name="input_identity_providers"></a> [identity\_providers](#input\_identity\_providers) | Map of identity providers to set in the trust relationship | <pre>map(object({<br>    type     = string<br>    arn      = optional(string)<br>    host     = optional(string)<br>    audience = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_project_owner"></a> [project\_owner](#input\_project\_owner) | Project owner name | `string` | `""` | no |
| <a name="input_role_manager_tag"></a> [role\_manager\_tag](#input\_role\_manager\_tag) | Role name tag between account name and project name. Eg: <role\_prefix><account\_name>-Mng-<project\_name> | `string` | `"-Mng-"` | no |
| <a name="input_role_prefix"></a> [role\_prefix](#input\_role\_prefix) | Role name prefix | `string` | `"MW-"` | no |

## Permissions

The minimum permissions to fully manage this module are the following. It is
recommended to specify the account ID, the region and the resource names
configured as the name of the DynamoDB table and S3 bucket.

If you don't want to specify the identity providers ARNs, it could be retrieved
at runtime, but it needs to define the `host` and it needs the extra permission
`iam:ListOpenIDConnectProviders` (check the following json, at the statement
with `Sid` `IAMOpenIDConnectListPermissions`)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "DynamoDBPermissions",
      "Effect": "Allow",
      "Action": [
        "dynamodb:CreateTable",
        "dynamodb:DeleteTable",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeTable",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UntagResource"
      ],
      "Resource": "arn:aws:dynamodb:<REGION>:<ACCOUNT_ID>:table/<DYNAMO_DB_TABLE_NAME>"
    },
    {
      "Sid": "IAMRolesPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:AttachRolePolicy",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:DetachRolePolicy",
        "iam:GetRole",
        "iam:ListAttachedRolePolicies",
        "iam:ListInstanceProfilesForRole",
        "iam:ListRolePolicies",
        "iam:TagRole",
        "iam:UntagRole",
        "iam:UpdateAssumeRolePolicy"
      ],
      "Resource": "arn:aws:iam::<ACCOUNT_ID>:role/<ROLE_PREFIX>*"
    },
    {
      "Sid": "IAMPolicyPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:CreatePolicy",
        "iam:DeletePolicy",
        "iam:GetPolicy",
        "iam:GetPolicyVersion",
        "iam:ListPolicyVersions",
        "iam:TagPolicy",
        "iam:UntagPolicy"
      ],
      "Resource": "arn:aws:iam::<ACCOUNT_ID>:policy/<ROLE_PREFIX>*"
    },
    {
      "Sid": "IAMOpenIDConnectPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:GetOpenIDConnectProvider"
      ],
      "Resource": "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/<PROVIDER_NAME>"
    },
    {
      "Sid": "IAMOpenIDConnectListPermissions",
      "Effect": "Allow",
      "Action": [
          "iam:ListOpenIDConnectProviders"
      ],
      "Resource": "*"
    },
    {
      "Sid": "ResourceGroupPermissions",
      "Effect": "Allow",
      "Action": [
        "resource-groups:CreateGroup",
        "resource-groups:DeleteGroup",
        "resource-groups:GetGroup",
        "resource-groups:GetGroupConfiguration",
        "resource-groups:GetGroupQuery",
        "resource-groups:GetTags",
        "resource-groups:Tag"
      ],
      "Resource": "arn:aws:resource-groups:<REGION>:<ACCOUNT_ID>:group/TerraformSharedStateResources"
    },
    {
      "Sid": "S3Permissions",
      "Effect": "Allow",
      "Action": [
        "s3:CreateBucket",
        "s3:DeleteBucket",
        "s3:GetAccelerateConfiguration",
        "s3:GetBucketAcl",
        "s3:GetBucketCORS",
        "s3:GetBucketLogging",
        "s3:GetBucketObjectLockConfiguration",
        "s3:GetBucketPolicy",
        "s3:GetBucketRequestPayment",
        "s3:GetBucketTagging",
        "s3:GetBucketVersioning",
        "s3:GetBucketWebsite",
        "s3:GetEncryptionConfiguration",
        "s3:GetLifecycleConfiguration",
        "s3:GetObject",
        "s3:GetReplicationConfiguration",
        "s3:ListBucket",
        "s3:PutBucketTagging"
      ],
      "Resource": "arn:aws:s3:::<BUCKET_NAME>"
    },
    {
      "Sid": "GetCallerIdentityPermission",
      "Effect": "Allow",
      "Action": [
        "sts:GetCallerIdentity"
      ],
      "Resource": "*"
    }
  ]
}
```

### Example

To create all the resources shown in the usage section you can use the following
permissions

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "DynamoDBPermissions",
      "Effect": "Allow",
      "Action": [
        "dynamodb:CreateTable",
        "dynamodb:DeleteTable",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeTable",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UntagResource"
      ],
      "Resource": "arn:aws:dynamodb:*:*:table/TerraformSharedState"
    },
    {
      "Sid": "IAMRolesPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:AttachRolePolicy",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:DetachRolePolicy",
        "iam:GetRole",
        "iam:ListAttachedRolePolicies",
        "iam:ListInstanceProfilesForRole",
        "iam:ListRolePolicies",
        "iam:TagRole",
        "iam:UntagRole",
        "iam:UpdateAssumeRolePolicy"
      ],
      "Resource": "arn:aws:iam::*:role/MW-*"
    },
    {
      "Sid": "IAMPolicyPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:CreatePolicy",
        "iam:DeletePolicy",
        "iam:GetPolicy",
        "iam:GetPolicyVersion",
        "iam:ListPolicyVersions",
        "iam:TagPolicy",
        "iam:UntagPolicy"
      ],
      "Resource": "arn:aws:iam::*:policy/MW-*"
    },
    {
      "Sid": "IAMOpenIDConnectPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:GetOpenIDConnectProvider"
      ],
      "Resource": "arn:aws:iam::*:oidc-provider/*"
    },
    {
      "Sid": "IAMOpenIDConnectListPermissions",
      "Effect": "Allow",
      "Action": [
          "iam:ListOpenIDConnectProviders"
      ],
      "Resource": "*"
    },
    {
      "Sid": "ResourceGroupPermissions",
      "Effect": "Allow",
      "Action": [
        "resource-groups:CreateGroup",
        "resource-groups:DeleteGroup",
        "resource-groups:GetGroup",
        "resource-groups:GetGroupConfiguration",
        "resource-groups:GetGroupQuery",
        "resource-groups:GetTags",
        "resource-groups:Tag"
      ],
      "Resource": "arn:aws:resource-groups:*:*:group/TerraformSharedStateResources"
    },
    {
      "Sid": "S3Permissions",
      "Effect": "Allow",
      "Action": [
        "s3:CreateBucket",
        "s3:DeleteBucket",
        "s3:GetAccelerateConfiguration",
        "s3:GetBucketAcl",
        "s3:GetBucketCORS",
        "s3:GetBucketLogging",
        "s3:GetBucketObjectLockConfiguration",
        "s3:GetBucketPolicy",
        "s3:GetBucketRequestPayment",
        "s3:GetBucketTagging",
        "s3:GetBucketVersioning",
        "s3:GetBucketWebsite",
        "s3:GetEncryptionConfiguration",
        "s3:GetLifecycleConfiguration",
        "s3:GetObject",
        "s3:GetReplicationConfiguration",
        "s3:ListBucket",
        "s3:PutBucketTagging"
      ],
      "Resource": "arn:aws:s3:::terraform-state-bucket"
    },
    {
      "Sid": "GetCallerIdentityPermission",
      "Effect": "Allow",
      "Action": [
        "sts:GetCallerIdentity"
      ],
      "Resource": "*"
    }
  ]
}
```
<!-- END_TF_DOCS -->