resource "aws_dynamodb_table" "this" {
  name           = var.dynamodb_table_name
  billing_mode   = var.dynamodb_table_billing_mode
  hash_key       = "LockID"
  read_capacity  = var.dynamodb_table_billing_mode == "PROVISIONED" ? var.dynamodb_read_capacity : null
  write_capacity = var.dynamodb_table_billing_mode == "PROVISIONED" ? var.dynamodb_write_capacity : null


  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = var.dynamodb_table_name
  }
}
