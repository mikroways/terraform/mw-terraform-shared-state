resource "aws_resourcegroups_group" "terraform_infraestructure" {
  name = "TerraformSharedStateResources"

  resource_query {
    query = <<JSON
{
  "ResourceTypeFilters": [
    "AWS::AllSupported"
  ],
  "TagFilters": [
    {
      "Key": "Managed-By",
      "Values": [
        "Terraform"
      ]
    },
    {
      "Key": "Repository",
      "Values": [
        "${var.git_repository}"
      ]
    }
  ]
}
JSON
  }

  tags = {
    Name = "TerraformSharedStateResources"
  }
}
