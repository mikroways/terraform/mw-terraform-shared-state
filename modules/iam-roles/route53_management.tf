#########
# Policy
#########
data "aws_iam_policy_document" "route53_management" {
  count = var.route53_management_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:CreateSecurityGroup",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeVpcs",
      "ec2:DescribeAvailabilityZones",
      "ec2:DescribeSecurityGroupRules",
      "ec2:DescribeVpcAttribute",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:DeleteSecurityGroup",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress",
      "ec2:CreateTags",
      "ram:AssociateResourceShare",
      "ram:CreateResourceShare",
      "ram:DeleteResourceShare",
      "ram:DisassociateResourceShare",
      "ram:GetResourceShareAssociations",
      "ram:GetResourceShares",
      "ram:ListResourceSharePermissions",
      "ram:ListResources",
      "ram:TagResource",
      "ram:UpdateResourceShare",
      "ram:EnableSharingWithAwsOrganization",
      "route53:AssociateVPCWithHostedZone",
      "route53:CreateHostedZone",
      "route53:GetChange",
      "route53:CreateVPCAssociationAuthorization",
      "route53:GetHostedZone",
      "route53:ListHostedZones",
      "route53:ListTagsForResource",
      "route53:GetHostedZoneCount",
      "route53:ListHostedZonesByName",
      "route53:ListResourceRecordSets",
      "route53:DeleteHostedZone",
      "route53:ListVPCAssociationAuthorizations",
      "route53:ListHostedZonesByVPC",
      "route53:DeleteVPCAssociationAuthorization",
      "route53:ChangeResourceRecordSets",
      "route53domains:ListDomains",
      "route53domains:ListOperations",
      "route53domains:RegisterDomain",
      "route53resolver:AssociateResolverEndpointIpAddress",
      "route53resolver:AssociateResolverRule",
      "route53resolver:CreateResolverEndpoint",
      "route53resolver:CreateResolverRule",
      "route53resolver:GetResolverConfig",
      "route53resolver:GetResolverEndpoint",
      "route53resolver:GetResolverRule",
      "route53resolver:DisassociateResolverRule",
      "route53resolver:DeleteResolverEndpoint",
      "route53resolver:GetResolverRuleAssociation",
      "route53resolver:GetResolverRulePolicy",
      "route53resolver:ListResolverConfigs",
      "route53resolver:ListResolverEndpointIpAddresses",
      "route53resolver:ListResolverEndpoints",
      "route53resolver:ListResolverRules",
      "route53resolver:ListTagsForResource",
      "route53resolver:PutResolverRulePolicy",
      "route53resolver:TagResource",
      "route53resolver:UntagResource",
      "route53resolver:UpdateResolverConfig",
      "route53resolver:UpdateResolverRule",
      "route53resolver:ListResourceTags",
      "route53resolver:DeleteResolverRule",
      "route53:UpdateHostedZoneComment",
    ]
  }
  dynamic "statement" {
    for_each = var.route53_management_extra_actions
    content {
      sid       = "Statement${title(statement.key)}"
      effect    = "Allow"
      resources = statement.value.resources
      actions   = statement.value.actions
    }
  }
}

resource "aws_iam_policy" "route53_management" {
  count       = var.route53_management_policy ? 1 : 0
  name        = "${var.role_prefix}Route53Management"
  path        = "/"
  description = "Least privilege policy required to manage DNS in Route53"
  policy      = data.aws_iam_policy_document.route53_management[0].json
}

###############
# Account role
###############
resource "aws_iam_role" "route53_management" {
  count = local.create_route53_management_role ? 1 : 0
  name  = "${var.role_prefix}Route53Management"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringEquals = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.route53_management_role.repo_paths, var.route53_management_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.route53_management_role.include_pull_request ? [
                  for path in var.route53_management_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}Route53Management"
  }
}


resource "aws_iam_role_policy_attachment" "route53_management_role_attach" {
  count      = local.create_route53_management_role ? 1 : 0
  role       = aws_iam_role.route53_management[0].name
  policy_arn = aws_iam_policy.route53_management[0].arn
}
