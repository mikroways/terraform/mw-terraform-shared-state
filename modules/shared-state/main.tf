terraform {
  required_version = ">= 1.5.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.50.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = merge(
      { "Managed-by" = "Terraform" },
      var.git_repository == null ? {} : { "Repository" = var.git_repository },
      var.project_owner == null ? {} : { "Owner" = var.project_owner }
    )
  }
}
