#########
# Policy
#########
data "aws_iam_policy_document" "kms_policy" {
  count = var.kms_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "kms:Decrypt",
      "kms:Encrypt"
    ]
  }
}

resource "aws_iam_policy" "kms_policy" {
  count       = var.kms_policy ? 1 : 0
  name        = "${var.role_prefix}KMSPolicy"
  path        = "/"
  description = "Least privilege policy required to manage and access KMS keys"
  policy      = data.aws_iam_policy_document.kms_policy[0].json
}

######################
# Access Account role
######################

resource "aws_iam_role" "kms_access" {
  count = local.create_kms_access_role ? 1 : 0
  name  = "${var.role_prefix}KMSAccess"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringEquals = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.kms_access_role.repo_paths, var.kms_access_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.kms_access_role.include_pull_request ? [
                  for path in var.kms_access_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}KMSAccess"
  }
}

resource "aws_iam_role_policy_attachment" "kms_access_role_attach" {
  count      = local.create_kms_access_role ? 1 : 0
  role       = aws_iam_role.kms_access[0].name
  policy_arn = aws_iam_policy.kms_policy[0].arn
}

##########################
# Management Account role
##########################

resource "aws_iam_role" "kms_management" {
  count = local.create_kms_management_role ? 1 : 0
  name  = "${var.role_prefix}KMSManagement"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringEquals = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.kms_management_role.repo_paths, var.kms_management_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.kms_management_role.include_pull_request ? [
                  for path in var.kms_management_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}KMSManagement"
  }
}

resource "aws_iam_role_policy_attachment" "kms_management_role_attach" {
  count      = local.create_kms_management_role ? 1 : 0
  role       = aws_iam_role.kms_management[0].name
  policy_arn = aws_iam_policy.kms_policy[0].arn
}