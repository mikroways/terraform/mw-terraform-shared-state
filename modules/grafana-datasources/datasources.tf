##############################
# Metric datasource resources
##############################
resource "random_uuid" "metrics" {
  count = var.configure_datasource_conections ? 1 : 0
}

resource "grafana_data_source" "metrics" {
  count               = var.metrics_create ? 1 : 0
  type                = "prometheus"
  name                = "${title(var.client_name)} - Metrics"
  uid                 = local.metrics_datasource_uid
  url                 = var.metrics_url
  basic_auth_enabled  = true
  basic_auth_username = var.metrics_username

  json_data_encoded = jsonencode(merge(
    {
      httpMethod    = "POST"
      manageAlerts  = false
      oauthPassThru = false
      sigV4Auth     = false
    },
    local.exemplars_to_traces,
    var.metrics_extra_json_data,
  ))

  secure_json_data_encoded = jsonencode(merge(
    {
      basicAuthPassword = var.metrics_password
    },
    var.metrics_extra_secure_json_data,
  ))
}

############################
# Logs datasource resources
############################
resource "random_uuid" "logs" {
  count = var.configure_datasource_conections ? 1 : 0
}

resource "grafana_data_source" "logs" {
  count               = var.logs_create ? 1 : 0
  type                = "loki"
  name                = "${title(var.client_name)} - Logs"
  uid                 = local.logs_datasource_uid
  url                 = var.logs_url
  basic_auth_enabled  = true
  basic_auth_username = var.logs_username

  json_data_encoded = jsonencode(merge(
    {
      manageAlerts  = false
      oauthPassThru = false
    },
    local.logs_to_traces,
    var.logs_extra_json_data,
  ))

  secure_json_data_encoded = jsonencode(merge(
    {
      basicAuthPassword = var.logs_password
    },
    var.logs_extra_secure_json_data,
  ))
}

##############################
# Traces datasource resources
##############################
resource "random_uuid" "traces" {
  count = var.configure_datasource_conections ? 1 : 0
}

resource "grafana_data_source" "traces" {
  count               = var.traces_create ? 1 : 0
  type                = "tempo"
  name                = "${title(var.client_name)} - Traces"
  uid                 = local.traces_datasource_uid
  url                 = var.traces_url
  basic_auth_enabled  = true
  basic_auth_username = var.traces_username

  json_data_encoded = jsonencode(merge(
    {
      search = {
        filters = [
          {
            id       = "service-name"
            operator = "="
            scope    = "resource"
            tag      = "service.name"
          },
          {
            id       = "span-name"
            operator = "="
            scope    = "span"
            tag      = "name"
          },
        ],
      },
    },
    local.traces_to_logs,
    local.traces_to_metrics,
    local.traces_service_map,
    var.traces_extra_json_data,
  ))

  secure_json_data_encoded = jsonencode(merge(
    {
      basicAuthPassword = var.traces_password
    },
    var.traces_extra_secure_json_data,
  ))
}
