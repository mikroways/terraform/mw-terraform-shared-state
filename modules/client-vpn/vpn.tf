data "awsutils_ec2_client_vpn_export_client_config" "vpn" {
  id = aws_ec2_client_vpn_endpoint.vpn.id
}

resource "aws_acm_certificate" "vpn_server" {
  certificate_body  = file(var.server_crt_file_path)
  private_key       = file(var.server_private_crt_file_path)
  certificate_chain = file(var.ca_file_path)
}

resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description            = "VPN endpoint"
  client_cidr_block      = var.client_cidr_block
  split_tunnel           = true
  server_certificate_arn = aws_acm_certificate.vpn_server.arn
  security_group_ids     = [aws_security_group.vpn_access.id]
  vpc_id                 = var.vpc_id
  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = aws_acm_certificate.vpn_server.arn
  }
  connection_log_options {
    enabled = false
  }
  tags = {
    Name = "VPN"
  }
}

resource "aws_security_group" "vpn_access" {
  vpc_id = var.vpc_id
  name   = "vpn-sg"
  ingress {
    from_port   = 3389
    protocol    = "TCP"
    to_port     = 3389
    cidr_blocks = ["0.0.0.0/0"]
    description = "Incoming RDP"
  }
  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "VPN"
  }
}

resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {
  for_each               = var.vpc_subnets
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id              = each.key
  lifecycle {
    // The issue why we are ignoring changes is that on every change
    // terraform screws up most of the vpn assosciations
    // see: https://github.com/hashicorp/terraform-provider-aws/issues/14717
    ignore_changes = [subnet_id]
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  for_each               = var.vpc_subnets
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr    = each.value
  authorize_all_groups   = true
}
