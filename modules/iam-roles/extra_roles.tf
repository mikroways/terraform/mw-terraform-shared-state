##########
# Policy #
##########

data "aws_iam_policy_document" "extra_roles" {
  for_each = var.extra_roles
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions   = each.value.actions
  }
}

resource "aws_iam_policy" "extra_roles" {
  for_each    = var.extra_roles
  name        = "${var.role_prefix}${each.key}"
  path        = "/"
  description = "Least privilege policy required for role ${each.key}"
  policy      = data.aws_iam_policy_document.extra_roles[each.key].json
}

################
# Account role #
################

resource "aws_iam_role" "extra_roles" {
  for_each = var.extra_roles
  name     = "${var.role_prefix}${each.key}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"

    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringLike = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(each.value.repo_paths, each.value.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && each.value.include_pull_request ? [
                  for path in each.value.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}${each.key}"
  }
}

resource "aws_iam_role_policy_attachment" "extra_roles_attach" {
  for_each   = var.extra_roles
  role       = aws_iam_role.extra_roles[each.key].name
  policy_arn = aws_iam_policy.extra_roles[each.key].arn
}

