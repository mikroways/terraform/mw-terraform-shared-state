##########
# Policy #
##########

data "aws_iam_policy_document" "roles_management" {
  count = var.roles_management_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "sts:GetCallerIdentity",
      "iam:AttachRolePolicy",
      "iam:CreateOpenIDConnectProvider",
      "iam:CreatePolicy",
      "iam:CreatePolicyVersion",
      "iam:CreateRole",
      "iam:DeleteOpenIDConnectProvider",
      "iam:DeletePolicy",
      "iam:DeletePolicyVersion",
      "iam:DeleteRole",
      "iam:DetachRolePolicy",
      "iam:GetOpenIDConnectProvider",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
      "iam:GetRole",
      "iam:ListAttachedRolePolicies",
      "iam:ListInstanceProfilesForRole",
      "iam:ListPolicyVersions",
      "iam:ListRolePolicies",
      "iam:TagOpenIDConnectProvider",
      "iam:UpdateAssumeRolePolicy",
      "iam:TagRole",
      "iam:ListRoles",
      "iam:ListPolicies",
      "iam:ListOpenIDConnectProviders",
      "iam:AddClientIDToOpenIDConnectProvider",
      "iam:PutRolePolicy",
      "iam:GetRolePolicy",
      "iam:CreateUser",
      "iam:ListUsers",
      "iam:CreateAccessKey",
      "iam:PutUserPolicy",
      "iam:TagUser",
      "iam:ListAccessKeys",
      "iam:ListSigningCertificates",
      "iam:ListSSHPublicKeys",
      "iam:ListServiceSpecificCredentials",
      "iam:ListMFADevices",
      "iam:ListUserPolicies",
      "iam:ListAttachedUserPolicies",
      "iam:ListGroupsForUser",
      "iam:DeleteUser",
      "iam:AttachUserPolicy",
      "eks:DescribeCluster",
      "iam:DeleteRolePolicy",
      "iam:TagPolicy"
    ]
  }
  dynamic "statement" {
    for_each = var.roles_management_extra_actions
    content {
      sid       = "Statement${title(statement.key)}"
      effect    = "Allow"
      resources = statement.value.resources
      actions   = statement.value.actions
    }
  }
}

resource "aws_iam_policy" "roles_management" {
  count       = var.roles_management_policy ? 1 : 0
  name        = "${var.role_prefix}RolesManagement"
  path        = "/"
  description = "Least privilege policy required to create required IAM Roles and Policies"
  policy      = data.aws_iam_policy_document.roles_management[0].json
}

################
# Account role #
################

resource "aws_iam_role" "roles_management" {
  count = local.create_roles_management_role ? 1 : 0
  name  = "${var.role_prefix}RolesManagement"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"

    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringLike = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.roles_management_role.repo_paths, var.roles_management_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.roles_management_role.include_pull_request ? [
                  for path in var.roles_management_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}RolesManagement"
  }
}

resource "aws_iam_role_policy_attachment" "roles_management_role_attach" {
  count      = local.create_roles_management_role ? 1 : 0
  role       = aws_iam_role.roles_management[0].name
  policy_arn = aws_iam_policy.roles_management[0].arn
}

