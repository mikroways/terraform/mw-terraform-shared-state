module "iam-roles" {
  source = "gitlab.com/mikroways/iam-roles/aws"

  network_management_policy = true
  network_management_role = {
    repo_paths    = ["org/aws-networking"]
    repo_branches = ["main"]
  }

  eks_access_policy = true
  eks_access_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  kms_policy = true

  eks_management_policy = true
  eks_management_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  roles_management_policy = true
  roles_management_role = {
    repo_paths    = ["org/aws-account-access"]
    repo_branches = ["main"]
  }

  extra_services_policy = true
  extra_services_policy_actions = [
    "ses:sendRawEmail"
  ]
  extra_services_role = {
    repo_paths    = ["org/aws-eks"]
    repo_branches = ["main"]
  }

  identity_providers = {
    github = {
      type = "github"
      arn  = "arn:aws:iam::xxxxxxxxxxxx:oidc-provider/token.actions.githubusercontent.com"
    }
  }
}
