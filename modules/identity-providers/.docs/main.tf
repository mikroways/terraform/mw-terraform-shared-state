module "identity-providers" {
  source = "gitlab.com/mikroways/identity-providers/aws"

  identity_providers = {
    github = {
      type = "github"
      thumbprints = ["b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a"]
    }
    gitlab = {
      type = "gitlab"
      audiences = ["custom.audience"]
    }
    gitlab-example = {
      type = "gitlab"
      host = "gitlab.example"
    }
  }
}
