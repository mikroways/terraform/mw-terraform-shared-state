## Permissions

The minimum permissions to fully manage this module are the following. It is
recommended to specify the account ID, the region and the resource names
configured as the name of the DynamoDB table and S3 bucket.

If you don't want to specify the identity providers ARNs, it could be retrieved
at runtime, but it needs to define the `host` and it needs the extra permission
`iam:ListOpenIDConnectProviders` (check the following json, at the statement
with `Sid` `IAMOpenIDConnectListPermissions`)