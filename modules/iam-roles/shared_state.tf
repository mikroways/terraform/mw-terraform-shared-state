locals {
  aws_region = (
    var.shared_state_role != null &&
    try(var.shared_state_role.aws_region, "*") != null
  ) ? var.shared_state_role.aws_region : "*"

  aws_account = (
    var.shared_state_role != null &&
    try(var.shared_state_role.aws_account, "*") != null
  ) ? var.shared_state_role.aws_account : "*"

  dynamodb_table_name = (
    var.shared_state_role != null &&
    try(var.shared_state_role.dynamodb_table_name, "*") != null
  ) ? var.shared_state_role.dynamodb_table_name : "*"
  s3_bucket_name = (
    var.shared_state_role != null &&
    try(var.shared_state_role.s3_bucket_name, "*") != null
  ) ? var.shared_state_role.s3_bucket_name : "*"
  oidc_provider_name = (
    var.shared_state_role != null &&
    try(var.shared_state_role.oidc_provider_name, "*") != null
  ) ? var.shared_state_role.oidc_provider_name : "*"
}

#########
# Policy
#########
data "aws_iam_policy_document" "shared_state" {
  count = var.shared_state_policy ? 1 : 0

  statement {
    sid    = "DynamoDBPermissions"
    effect = "Allow"
    actions = [
      "dynamodb:CreateTable",
      "dynamodb:DeleteTable",
      "dynamodb:DescribeContinuousBackups",
      "dynamodb:DescribeTable",
      "dynamodb:DescribeTimeToLive",
      "dynamodb:ListTagsOfResource",
      "dynamodb:TagResource",
      "dynamodb:UntagResource"
    ]
    resources = ["arn:aws:dynamodb:${local.aws_region}:${local.aws_account}:table/${local.dynamodb_table_name}"]
  }

  statement {
    sid    = "IAMRolesPermissions"
    effect = "Allow"
    actions = [
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:DetachRolePolicy",
      "iam:GetRole",
      "iam:ListAttachedRolePolicies",
      "iam:ListInstanceProfilesForRole",
      "iam:ListRolePolicies",
      "iam:TagRole",
      "iam:UntagRole",
      "iam:UpdateAssumeRolePolicy"
    ]
    resources = ["arn:aws:iam::${local.aws_account}:role/${var.role_prefix}*"]
  }

  statement {
    sid    = "IAMPolicyPermissions"
    effect = "Allow"
    actions = [
      "iam:CreatePolicy",
      "iam:DeletePolicy",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
      "iam:ListPolicyVersions",
      "iam:TagPolicy",
      "iam:UntagPolicy",
      "iam:DeletePolicyVersion",
      "iam:DeletePolicy"
    ]
    resources = ["arn:aws:iam::${local.aws_account}:policy/${var.role_prefix}*"]
  }

  statement {
    sid    = "IAMOpenIDConnectPermissions"
    effect = "Allow"
    actions = [
      "iam:GetOpenIDConnectProvider"
    ]
    resources = ["arn:aws:iam::${local.aws_account}:oidc-provider/${local.oidc_provider_name}"]
  }

  statement {
    sid    = "IAMOpenIDConnectListPermissions"
    effect = "Allow"
    actions = [
      "iam:ListOpenIDConnectProviders"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "ResourceGroupPermissions"
    effect = "Allow"
    actions = [
      "resource-groups:CreateGroup",
      "resource-groups:DeleteGroup",
      "resource-groups:GetGroup",
      "resource-groups:GetGroupConfiguration",
      "resource-groups:GetGroupQuery",
      "resource-groups:GetTags",
      "resource-groups:Tag"
    ]
    resources = ["arn:aws:resource-groups:${local.aws_region}:${local.aws_account}:group/TerraformSharedStateResources"]
  }

  statement {
    sid    = "S3Permissions"
    effect = "Allow"
    actions = [
      "s3:CreateBucket",
      "s3:DeleteBucket",
      "s3:GetAccelerateConfiguration",
      "s3:GetBucketAcl",
      "s3:GetBucketCORS",
      "s3:GetBucketLogging",
      "s3:GetBucketObjectLockConfiguration",
      "s3:GetBucketPolicy",
      "s3:GetBucketRequestPayment",
      "s3:GetBucketTagging",
      "s3:GetBucketVersioning",
      "s3:GetBucketWebsite",
      "s3:GetEncryptionConfiguration",
      "s3:GetLifecycleConfiguration",
      "s3:GetObject",
      "s3:GetReplicationConfiguration",
      "s3:ListBucket",
      "s3:PutBucketTagging"
    ]
    resources = ["arn:aws:s3:::${local.s3_bucket_name}"]
  }

  statement {
    sid    = "GetCallerIdentityPermission"
    effect = "Allow"
    actions = [
      "sts:GetCallerIdentity"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "shared_state" {
  count       = var.shared_state_policy ? 1 : 0
  name        = "${var.role_prefix}SharedState"
  path        = "/"
  description = "Least privilege policy required to use the module shared-state"
  policy      = data.aws_iam_policy_document.shared_state[0].json
}

###############
# Account role
###############

resource "aws_iam_role" "shared_state" {
  count = local.create_shared_state_role ? 1 : 0
  name  = "${var.role_prefix}SharedState"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"

    Statement = concat(
      [
        for _, identity_provider in local.identity_providers :
        {
          Action = "sts:AssumeRoleWithWebIdentity"
          Effect = "Allow"
          Sid    = ""
          Principal = {
            Federated = identity_provider.arn
          }
          Condition = {
            StringLike = {
              "${identity_provider.host}:aud" = identity_provider.audience
              "${identity_provider.host}:sub" = concat(
                [
                  for oidc_subjects in setproduct(var.shared_state_role.repo_paths, var.shared_state_role.repo_branches) :
                  "${identity_provider.subject_prefix}:${oidc_subjects[0]}:${identity_provider.subject_ref}${oidc_subjects[1]}"
                ],
                identity_provider.type == "github" && var.shared_state_role.include_pull_request ? [
                  for path in var.shared_state_role.repo_paths :
                  "${identity_provider.subject_prefix}:${path}:pull_request"
                ] : []
              )
            }
          }
        }
      ],
      local.assume_role_from_accounts
    )
  })

  tags = {
    Name = "${var.role_prefix}SharedState"
  }
}

resource "aws_iam_role_policy_attachment" "shared_state_role_attach" {
  count      = local.create_shared_state_role ? 1 : 0
  role       = aws_iam_role.shared_state[0].name
  policy_arn = aws_iam_policy.shared_state[0].arn
}

