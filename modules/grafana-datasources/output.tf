output "metrics_datasource_uid" {
  description = "Metrics datasource identifier"
  value       = var.metrics_create ? grafana_data_source.metrics[0].id : null
}

output "logs_datasource_uid" {
  description = "Logs datasource identifier"
  value       = var.logs_create ? grafana_data_source.logs[0].id : null
}

output "traces_datasource_uid" {
  description = "Traces datasource identifier"
  value       = var.traces_create ? grafana_data_source.traces[0].id : null
}
