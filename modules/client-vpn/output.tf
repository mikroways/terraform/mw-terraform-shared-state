output "sg_id" {
  value = aws_security_group.vpn_access.id
}

output "certificate_arn" {
  value = aws_acm_certificate.vpn_server.arn
}

output "vpn_endpoint_arn" {
  value       = join("", aws_ec2_client_vpn_endpoint.vpn[*].arn)
  description = "The ARN of the Client VPN Endpoint Connection."
}

output "vpn_endpoint_id" {
  value       = join("", aws_ec2_client_vpn_endpoint.vpn[*].id)
  description = "The ID of the Client VPN Endpoint Connection."
}

output "vpn_endpoint_dns_name" {
  value       = join("", aws_ec2_client_vpn_endpoint.vpn[*].dns_name)
  description = "The DNS Name of the Client VPN Endpoint Connection."
}

output "client_configuration" {
  value       = join("", data.awsutils_ec2_client_vpn_export_client_config.vpn[*].client_configuration)
  description = "VPN Client Configuration data."
}