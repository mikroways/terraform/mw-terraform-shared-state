variable "git_repository" {
  type        = string
  default     = ""
  description = "Terraform git repository"
}

variable "project_owner" {
  type        = string
  default     = ""
  description = "Project owner name"
}

variable "bucket_name" {
  type        = string
  description = "Bucket name for the terraform state"
}

variable "dynamodb_table_name" {
  type        = string
  description = "DynamoDB table name for the terraform state lock"
}

variable "dynamodb_table_billing_mode" {
  type        = string
  description = "DynamoDB table billing mode. Valid options 'PAY_PER_REQUEST' and 'PROVISIONED'"
  default     = "PAY_PER_REQUEST"
}

variable "dynamodb_read_capacity" {
  type        = string
  description = "DynamoDB read capacity. Only used if the billing_mode is PROVISIONED"
  default     = 1
}

variable "dynamodb_write_capacity" {
  type        = string
  description = "DynamoDB write capacity. Only used if the billing_mode is PROVISIONED"
  default     = 1
}

variable "create_roles" {
  type        = bool
  description = "Whether to create a role"
  default     = true
}

variable "role_prefix" {
  type        = string
  description = "Role name prefix"
  default     = "MW-"
}

variable "role_manager_tag" {
  type        = string
  description = "Role name tag between account name and project name. Eg: <role_prefix><account_name>-Mng-<project_name>"
  default     = "-Mng-"
}

variable "create_policy_wildcard_per_environment" {
  type        = bool
  description = "Whether to create a policy wildcard per environment"
  default     = true
}

variable "identity_providers" {
  type = map(object({
    type     = string
    arn      = optional(string)
    host     = optional(string)
    audience = optional(string)
  }))
  description = "Map of identity providers to set in the trust relationship"

  validation {
    condition     = alltrue([for idp in var.identity_providers : contains(["github", "gitlab"], idp.type)])
    error_message = "Valid values for identity_provider_type: 'github' or 'gitlab'"
  }

  validation {
    condition     = alltrue([for idp in var.identity_providers : length(idp.arn) > 0 if idp.arn != null])
    error_message = "The identity provider ARN couldn't be an empty string"
  }

  default = {}
}

variable "audience" {
  type        = string
  default     = "sts.amazonaws.com"
  description = "Terraform role trust relationship audience"
}

variable "environments" {
  type = list(
    object({
      name     = string
      audience = optional(string)
      projects = list(object({
        name                 = string
        git_project_path     = string
        git_project_branches = list(string)
        include_pull_request = optional(bool)
      }))
    })
  )
  description = "Account list"
}

############################
# Assume role from accounts
############################
variable "assume_role_from_accounts" {
  type        = list(string)
  default     = []
  description = "List of account to allow to assume roles"
}