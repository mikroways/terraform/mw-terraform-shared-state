## Permissions

The minimum permissions to fully manage this module are the following. It is
recommended to specify the account ID and the resource names of the identity
providers.