#########
# Policy
#########
data "aws_iam_policy_document" "cost_explorer" {
  count = var.billing_ce_policy ? 1 : 0
  statement {
    sid       = "Statement1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ce:DescribeReport",
      "ce:GetCostAndUsage",
      "ce:GetCostForecast",
      "ce:GetCostAndUsageWithResources",
      "ce:GetUsageForecast",
      "ce:GetDimensionValues",
      "ce:GetTags",
      "ce:GetCostCategories",
      "ce:GetSavingsPlansUtilization",
      "ce:GetSavingsPlansUtilizationDetails",
      "ce:GetSavingsPlansCoverage",
      "ce:ListCostCategoryDefinitions",
      "ce:DescribeCostCategoryDefinition",
      "ce:GetApproximateUsageRecords",
      "ce:GetReservationUtilization",
      "ce:ListCostAllocationTags",
      "ce:GetPreferences",
      "ce:GetReservationCoverage",
    ]
  }
}

resource "aws_iam_policy" "cost_explorer" {
  count       = var.billing_ce_policy ? 1 : 0
  name        = "${var.role_prefix}CostExplorerAccess"
  path        = "/"
  description = "Least privilege policy required for read only access to Cost Explorer"
  policy      = data.aws_iam_policy_document.cost_explorer[0].json
}

