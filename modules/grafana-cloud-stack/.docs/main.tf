module "example" {
  source = "gitlab.com/mikroways/stack/grafana"

  client_name = "example"

  create_stack_service_account = true
  create_read_access_policies  = true
  create_write_access_policies = true
}
