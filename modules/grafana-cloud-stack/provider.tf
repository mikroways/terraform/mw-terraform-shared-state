terraform {
  required_version = "~> 1.8.2"

  required_providers {
    grafana = {
      source  = "grafana/grafana"
      version = "2.18.0"
    }
  }
}
